#!/bin/bash

# Install every node package I use on any project.

npm install -g stylus
npm install -g uglify-js
npm install -g babel@5.8
npm install -g postcss-cli
npm install -g autoprefixer
npm install -g cordova
npm install -g http-server		# only because python's is really bad
