#!/bin/bash

# Dump all Skype chat logs with a given counterparty.

set -e

my_handle='gavin.cannizzaro'

who="${1:-bazwaldo35}"
dbfile="${2:-$APPDATA/Skype/$my_handle/main.db}"
out_dir="skype-schema"


# Dump schema (for dev)
if [ ! -d "$out_dir" ]; then
	mkdir -p "$out_dir"
	
	echo '.schema' \
		| sqlite3 "$dbfile" \
		| grep 'CREATE TABLE' \
		| cut -d' ' -f3 \
			  > "$out_dir/tables"

	cat "$out_dir/tables" | while read table; do
		echo ".schema $table" \
			| sqlite3 "$dbfile" \
			| sed "s/,/\n, /g" \
				  > "$out_dir/$table.sql"
	done
fi

echo '<table>'
echo "SELECT '<tr>' \
             || '<td>' \
             ||   (case cast (strftime('%w', timestamp) as integer)
                  when 0 then 'Sunday' \
                  when 1 then 'Monday' \
                  when 2 then 'Tuesday' \
                  when 3 then 'Wednesday' \
                  when 4 then 'Thursday' \
                  when 5 then 'Friday' \
                  else 'Saturday' end) \
             || ' '
             || strftime('%Y-%m-%d %H:%M:%S', datetime(timestamp, 'unixepoch')) || '</td>' \
             || '<td>' || author || '</td>' \
             || '<td>' || body_xml || '</td>' \
             || '</tr>' \
      FROM Messages \
      WHERE dialog_partner = '$who' \
      ORDER BY timestamp;" \
	 | sqlite3 "$dbfile"
echo '</table>'

exit 0
