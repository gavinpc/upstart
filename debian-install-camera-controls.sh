#!/bin/bash

# Install GUI for setting webcam properties. (v4l)

set -e

# Following https://www.techytalk.info/webcam-settings-control-ubuntu-linux/

# sudo add-apt-repository ppa:libv4l/ppa
# But
# ERROR: ppa 'libv4l/ppa' not found (use --login if private)

# This installed, though, and looks okay so far
sudo add-apt-repository ppa:pj-assis/ppa
sudo apt-get update
sudo apt-get install guvcview

# It appears that OBS can't read from the camera while this app is open.

exit 0
