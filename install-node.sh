#!/bin/sh

# I've gone through a lot of runaround to (1) install the latest node reliably
# and (2) install global packages without sudo (so that you can run them as
# user).  For (1), I used to use nvm.  Now it looks like this will do:

# c/o https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
# or https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-22-04#option-2-installing-node-js-with-apt-using-a-nodesource-ppa

# If you want version 6, you can do this:
# 
#curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
#
# But amazingly (or not), it turns out that version 6 breaks some things that
# were previously working.  I ran into this, for example
# (https://github.com/webpack/webpack/issues/2463).
#
# Ten node versions later...
# I pre-ran this command and have included the result in this repo.
# 
# curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
this_script_dir="$(cd "$(dirname "$0")" && pwd)"
sudo bash "$this_script_dir/nodesource_setup.sh"

sudo apt-get install -y nodejs

# For (2), see
# . ./node.setup.sh
