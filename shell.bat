@ECHO OFF

rem Run this as administrator to associate Emacs with all kinds of text files.

rem Assumes that `emacsclientw` and `runemacs` are in the same directory as
rem emacs, which is on the PATH.

rem See https://www.emacswiki.org/emacs/EmacsMsWindowsIntegration

rem The approach used here sometimes gives an error message such as "No
rem connection could be made because the target machine actively refused it,"
rem then it launches in a new instance, even if there's already one open.  I
rem haven't explored whether this might have anything to do with firewall
rem settings (which I doubt), because so far it appears to have to do with the
rem server file laying around from an earlier (presumably crashed) instance.
rem Deleting that file (~/.emacs.d/server/server) “resolves” this, although I
rem didn't have to do this in the past.
rem 
rem http://stackoverflow.com/q/25320064/4525

FOR /F "tokens=* USEBACKQ" %%F IN (`where emacs`) DO (
SET ep=%%~dpF
)

IF "%ep%"=="" (
   ECHO Couldn't find emacs on the PATH.
   EXIT /B 1
)

ECHO emacs path is %ep%

rem Note that %ep% will include the trailing slash.
ftype txtfile=%ep%emacsclientw.exe -na %ep%runemacs.exe "%%1" %%*
ftype XmlFile=%ep%emacsclientw.exe -na %ep%runemacs.exe "%%1" %%*
ftype CodeFile=%ep%emacsclientw.exe -na %ep%runemacs.exe "%%1" %%*
assoc .txt=txtfile
assoc .log=txtfile
assoc .config=txtfile
assoc .ini=txtfile
assoc .hgignore=txtfile
assoc .hgrc=txtfile
assoc .sql=CodeFile
assoc .org=CodeFile
assoc .md=CodeFile
assoc .gpg=CodeFile
assoc .xml=XmlFile
assoc .xsl=XmlFile
assoc .tex=CodeFile
assoc .el=CodeFile
assoc .cs=CodeFile
assoc .csproj=CodeFile
assoc .sln=CodeFile
assoc .js=CodeFile
assoc .css=CodeFile
assoc .styl=CodeFile
assoc .rnc=CodeFile
assoc .wiki=CodeFile
assoc .aspx=CodeFile

FOR /F "tokens=* USEBACKQ" %%F IN (`where bash`) DO (
SET bp=%%~dpF
)

ftype ShellScriptFile=%bp%bash.exe "%%1" %%*
assoc .sh=ShellScriptFile

