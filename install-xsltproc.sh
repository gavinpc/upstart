#!/bin/bash

# Install xsltproc on a Debian-based system.

set -e

sudo apt install xsltproc

exit 0
