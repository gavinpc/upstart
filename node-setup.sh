#!/bin/sh

# This allows you to install npm packages global without sudo, meaning that you
# can run them as user.

# c/o https://docs.npmjs.com/getting-started/fixing-npm-permissions
mkdir -p ~/.npm-global
npm config set prefix '~/.npm-global'
# You need to add to your PATH
#export PATH=~/.npm-global/bin:$PATH
