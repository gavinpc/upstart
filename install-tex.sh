#!/bin/bash

# 2022-06-04 I find this will do
sudo apt install texlive-latex-extra
exit 0

# I use TeX Live because it avoids the headaches of missing packages, in favor
# of other headaches.  Debian provides `texlive-full`, and you can use that:
# 
#    sudo apt-get install -y texlive-full
#
# But it'll be out of date, and it's hard to update individual packages.
# Instead, I get the "upstream" TeX Live, which avoids those headaches in favor
# of other headaches.
# 
# See also
# http://kevin.godby.org/2012/02/13/installing-tex-live-in-ubuntu/
# https://www.tug.org/texlive/doc/texlive-en/texlive-en.html#x1-270003.2.4

archive='install-tl-unx.tar.gz'
cd ~/Downloads
test -f "$archive" && mv -f "$archive" /tmp
wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
tar -xzf "$archive"
cd `tar -tzf $archive | sed -e 'N;s/^\(.*\).*\n\1.*$/\1\n\1/;D'`

echo "NOTE!"
echo 'You will be prompted to install TeX Live.'
echo 'Be SURE to "create" symlinks in standard directories" before installing!'
echo 'Press ENTER to continue.'
sudo ./install-tl

mkdir -p /opt
sudo ln -s /usr/local/texlive/2016/bin/* /opt/texbin

# TeX Live doesn't include some common commands, like `texi2dvi`.  These are in
# the `texinfo` package.
sudo apt-get install -y texinfo
