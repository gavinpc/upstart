# Linux setup

## setup

This involves getting Mercurial, because `upstart` is a Mercurial repository.

### Mercurial

I started with

```
#!sh
sudo apt-get install -y mercurial
```

then

```
#!sh
cd
hg clone https://bitbucket.org/gavinpc/upstart
```

Then I added the Emacs PPA at

https://launchpad.net/~ubuntu-elisp/+archive/ubuntu/ppa

and then

```
#!sh
sudo apt-get install -y emacs-snapshot
```

Then emacs was installed as a normal app.  I ran it, which created the `~/.emacs.d` directory.

```
#!sh
echo '(load "~/upstart")' > ~/.emacs.d/init.el
```

Then loading emacs installed a bunch of packages.

## system settings

I disable all system sounds.

### keyboard repeat rate

See `set-keyrate.sh`.

In Ubuntu 22.04 (and perhaps earlier), this setting is under “Accessibility” >
“Repeat Keys”.  Apparently repeat rate is reversed from what it used to be.
When I set the delay and repeat rate as low as the UI allows, `xset q` reports:

```
Keyboard Control:
  auto repeat:  on    key click percent:  0    LED mask:  00000000
  auto repeat delay:  100    repeat rate:  9e
```

See additional notes in the script.


### Alt+Space should be usable in emacs

`M-SPC` in emacs should run the command `just-one-space`.  But it's common for
that key (also known as “Alt+Space”) to open the “window menu,” a generic menu
provided by the window manager.  I do sometimes use that key for accessing the
window menu, but not nearly as often as I use `just-one-space`.  The emacs
binding should take precedence, and in some systems (X windows) it doesn't.

You can work around this by removing that keybinding.  Here, it is under System
Settings / Keyboard / Keyboard Shortcuts / Windows / Activate Window Menu.  This
could probably be scripted with `gsettings` using the path
`/org/gnome/desktop/wm/keybindings/activate-window-menu`

### Alt+Backtick (a.k.a. “Grave”) should be usable in Emacs

I use Alt+Backtick as an alias for `switch-to-buffer`.  On Ubuntu, this is used
to switch between windows within a group.  With a little help from [this
post](https://bugs.launchpad.net/unity-2d/+bug/992928/comments/4) and others, I
found that this changed it to `Win-Backtick`.

```sh
gsettings set org.gnome.desktop.wm.keybindings switch-group "['<Super>Grave']"
```

That said, I'd kind of prefer that Unity not do the window grouping in the first
place.  Although it is claimed in [this post](https://askubuntu.com/a/1193837)
that “you cannot do this,” I'm finding in 22.04 that multiple instances have
separate entries in the Alt-tab sequence, which is all I need.

So the following paragrah may be OBE, as I can simply disable the default
function that uses Alt+backtick.

In `dconf-editor`, I unset the shortcuts for `switch-group` and
`switch-group-backward`, where unset means unchecking “Use default value” _and_
setting the current value to an empty list, notated `[]`.

Older notes:
> Working from [this answer](https://superuser.com/a/860001), I found that
> `switch-application` and `reverse-switch-application` have two key
> combinations each, while `switch-window` and `reverse-switch-window` have
> none.  I moved the `Alt` versions to `window` so they behave like Windows, but
> you can still get the Mac-like behavior with the `Super` modifier.


### Ctrl+Alt+L

Ubuntu (or Mint, or Debian) binds `Ctrl+Alt+L` to “Lock Screen.”  This is
annoying because I sometimes accidentally type it.  I never use Lock Screen, but
on Windows it was Win+L, or what Linux calls “Super”+L.  But that's bound to
some debugging utility called Looking Glass.  I changed that to Alt+Super+L and
changed Lock Screen to Super+L.

## software

### Git

Yeah, I also need git.

```
#!sh
sudo apt-get install -y git
```

You need to tell git a name and email before you can commit anything.  I have
generally done this globally, though the email address could differ for
different repositories.

```
#!sh
git config --global user.name "Gavin Cannizzaro"
git config --global user.email "g@def.codes"
```

Following is moot now as I use SSH for pretty much everything.

```
#!sh
git config credential.helper store
```

So that you don't get prompted for username or password every time you push.

## settings

### Mercurial

Make `~/.hgrc` with at least

```
[ui]
username = gavin
```

But you'll also need your BitBucket `[auth]` settings.  Of course, I can't
include them here.

### Firefox

Pretty much same as those on Windows, except that you can leave auto updates
enabled.

### Tup

Put this in `~/.tupoptions`.  I never run the monitor without also updating.
```
#!conf
[monitor]
	autoupdate = true
```

## lagniappe

### converting a Mercurial repository to Git

“Fortunately or unfortunately, Git won over Mercurial.”

Says the Aussie Tatham Oddie in his
post
[“Nerd Corner: Convert a Mercurial (Hg) repo to Git, with full fidelity, on any OS”](https://blog.tatham.oddie.com.au/2014/03/24/nerd-corner-convert-a-mercurial-hg-repo-to-git-with-full-fidelity-on-any-os/).
Those steps worked for me in 2016 without fuss (and of course without the Azure
VM).  I also used an "authors" file so that git would have an email address for
the existing commits, which looks like:

```
gavin=gavin<gavin@willshake.net>
```

### enabling your touchpad without your touchpad

So I ended up in a wierd fix.  I use a wireless trackball because it's
*impossible* to type on this laptop without interference from the touchpad.
Don't tell me about palm settings---it don't work.

On Windows, the Synaptics driver lets you enable and disable the touchpad by
double-tapping in its corner.  It also lets you disable it automatically when
another pointing device is plugged in (which one always is, because of the
little wireless dongle).

But none of that stuff exists on Linux.  (Actually see the
section
["Disable touchpad on mouse detection"](https://wiki.archlinux.org/index.php/Touchpad_Synaptics#Disable_touchpad_on_mouse_detection) in
the Ubuntu docs.)

So I just set the Touchpad to "disabled".  But I was somewhere and hadn't
brought my trackball.  Okay, I thought, I'll just re-enable it using the
keyboard.

But it turns out that Cinnamon's system GUI is *completely* unusable with the
keyboard (and no one has noticed this for some reason).  There is absolutely no
way to get to that option.

Okay... then I'll just use the command line.  You can control this using a
program
called
[synclient](https://www.x.org/archive/X11R7.5/doc/man/man1/synclient.1.html),
or [xinput](http://askubuntu.com/a/530335).

But [it turns out](http://askubuntu.com/a/727148)

> `synclient` and `xinput` will not work if you are using gnome (or unity,
> cinnamon) environment, because it will override settings, so if you want
> `synclient` or `xinput` to take over these settings, you should disable that
> first:
>
> 1. install dconf-editor if not installed:
> 
>     apt-get install dconf-editor
> 
> 2. run dconf-editor
> 
>     dconf-editor 
> 
> 3. open the directory `/org/gnome/settings-daemon/plugins/mouse/` or
>    `/org/cinnamon/settings-daemon/plugins/mouse/`, and unclick the checkbox for
>    active.
> 
> 4. logout or reboot

Well, it turns out that the `dconf-editor` GUI is *also* unusable with the
keyboard.  [You can get around that as well](http://askubuntu.com/a/72093) by
using `gsettings`:

```
#!sh
gsettings set org.cinnamon.settings-daemon.plugins.mouse active false
```

And that all does in fact work, so you can say

```
#!sh
synclient TouchpadOff=0
```

### fixing your brightness controls

There's apparently a common problem starting in Mint 17
where
[the brightness controls don't work](https://www.reddit.com/r/linuxmint/comments/2fgngu/brightness_controlls_doesnt_work_in_linux_mint_17/).
I can attest.

The `xbacklight` package suggested
in [this thread](https://forums.linuxmint.com/viewtopic.php?t=173017) didn't
change anything.

The solution in [this thread](https://itsfoss.com/fix-brightness-ubuntu-1310/)
did work.  Note that the solution is different for non-Intel cards.

Add these lines to `/usr/share/X11/xorg.conf.d/20-intel.conf`, creating it if it
doesn't exist.  Then log off and back on.

```
#!conf
Section "Device"
        Identifier  "card0"
        Driver      "intel"
        Option      "Backlight"  "intel_backlight"
        BusID       "PCI:0:2:0"
EndSection
```

Now the brightness controls (at least those under Power Settings) should work.

### Org source editing problem

I had a problem where every time I try to edit a source block in Org mode
(`org-edit-special`), I would get

> remove: Wrong type argument: listp, t

and the mode wouldn't be loaded, and `C-'` wouldn't be bound to closing the edit
buffer.

Updating to the latest Org from `http://orgmode.org/elpa/` (8.3.6) (as
per
[this post](https://lists.gnu.org/archive/html/emacs-devel/2016-02/msg01426.html))
fixed it for me.

### HiDPI

Current versions of Mint don't handle 4K screens too well.  I messed with a few
settings to deal with that.

### Firefox touch scrolling

In Mint 18.1 touch gestures in Firefox select instead of scroll.  The fix
suggested [here](https://askubuntu.com/a/886914) works:

>     sudo gedit /usr/share/applications/firefox.desktop
>
> find the Exec line in the [Desktop Entry] section and change it to
>
>     Exec=env MOZ_USE_XINPUT2=1 firefox %u

### Getting SSH access

This is just basic knowledge of how to use SSH, but for the record, if you want
to get SSH access from a new machine, you'll need a machine that can already
access the remote target, and a way to get files to that machine:

1. Run `ssh-keygen` on the new machine, accepting all of the defaults.

2. Then send your `~/.ssh/id_rsa.pub` to the machine that already has access.

3. From there, you can use `scp` to put it on the remote, like `scp id_rsa.pub
   example.net:new.pub`.

4. Finally, `ssh` in and `cat new.pub >> ~/.ssh/authorized_keys`.

Then you should be able to `ssh` from the new machine!

## Fn lock

The top-row keys should be F1-12, not whatever the vendor thinks their alternate
functions should be.  Usually this is configured in the BIOS.  On the Framework
laptop, fn+Esc is fn lock, which persists over restarts.
