#!/bin/bash

# Install OBS studio on a Debian-based system.

set -e

# sudo apt install obs-studio
# except that the apt version crashed right away

# https://snapcraft.io/obs-studio
snap install obs-studio

# “For the best experience, you'll want to connect the following interfaces.”
# I have also run this
sudo snap connect obs-studio:alsa
sudo snap connect obs-studio:audio-record
sudo snap connect obs-studio:avahi-control
sudo snap connect obs-studio:camera
sudo snap connect obs-studio:jack1
sudo snap connect obs-studio:kernel-module-observe

exit 0
