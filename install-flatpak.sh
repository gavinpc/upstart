#!/bin/bash

# UPDATE: I've installed this, but I don't think I need it, as snap is already installed.
#
# Install flatpak.
# See https://flatpak.org/setup/Ubuntu
# Came across it here: https://linuxhint.com/install_obs_ubuntu/

set -e

sudo apt update
sudo apt install flatpak
sudo apt install gnome-software-plugin-flatpak

exit 0
