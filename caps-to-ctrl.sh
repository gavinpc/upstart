#!/bin/sh

# The first thing I do on any new system is map Caps Lock to Control.
# This method should work on Debian-based systems.

sed -iorig 's/XKBOPTIONS=""/XKBOPTIONS="ctrl:nocaps"/g' /etc/default/keyboard

# Apply the changes
setupcon

# See /usr/share/doc/keyboard-configuration/README.Debian
udevadm trigger --subsystem-match=input --action=change

echo "You need to restart now.  Don't fight it, just do it."
