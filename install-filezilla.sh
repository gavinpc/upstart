#!/bin/bash

# Install FileZilla on a Debian-based system.

set -e

sudo apt install filezilla

exit 0
