@ECHO OFF

rem Use Chocolatey to install all of the things I use regularly (if not always
rem willingly).

rem First, get the latest PowerShell.  This is not specifically needed for any
rem reason... except there's one case where a package has a bug that exists only
rem in earlier versions of PowerShell.  Might as well avoid that now.
choco upgrade -yes powershell

rem You probably need to reboot at this point, anyway.

ECHO Stick around... the following packages are missing checksums, so you'll
ECHO need to confirm their installation.

rem  Yes, there's an official `emacs`, which works, but you run into a problem
rem  pulling packages, where the main `.el` file has the word "closed" on the
rem  last line, and you end up with "Symbol's value as variable is void: closed"
rem  Google it.  It has something to do with bad TLS connections.  This build
rem  adds a "GnuTLS" tool, and doesn't have that problem.
choco install -yes emacs64

rem Does this automatically get 4.5.2?
choco install -yes dotnet4.6
choco install -yes f.lux
choco install -yes launchy-beta
choco install -yes linqpad
choco install -yes firefox
choco install -yes googlechrome
choco install -yes ie11

rem This is needed to build willshake.  Note per this comment (and others)
rem (https://chocolatey.org/packages/ffmpeg#comment-2714396394) that there's a
rem bug in the install script that requires either a patch or a more recent
rem version of PowerShell than the one... with which it doesn't work.
choco install -yes ffmpeg

rem Wait, I think these have checksums
choco install -yes thunderbird
rem See https://chocolatey.org/packages/sumatrapdf
rem Right now, they seem to have these reversed.
rem choco install -yes sumatrapdf
choco install -yes sumatrapdf.install


ECHO
ECHO Okay, the rest of the packages have checksums (last I checked), so you can
ECHO go now!
ECHO

rem Mercurial package is now problematic.  This did not work on latest Windows
rem 10.  I got errors either during install (something about AMD 64) or when
rem trying to run (something about a missing DLL).  I installed the VC 2008
rem redistributable (https://www.microsoft.com/en-us/download/details.aspx?id=2092)
rem and then Mercurial-4.9.1-x64.exe probably from
rem https://www.mercurial-scm.org/wiki/Download#Windows

choco install -yes hg

rem  This Git package includes basically a GNU/Unix environment like MSys, but
rem  it now puts those tools (except bash) in its `usr\bin` folder instead of
rem  its `bin`. Using `-params /GitAndUnixToolsOnPath` is supposed to include
rem  both, but it apparently didn't do anything differently, so I added the
rem  `usr\bin` to my `PATH` manually.  See `patheditor`
choco install -yes git -params /GitAndUnixToolsOnPath

choco install -yes nodejs
choco install -yes procexp
choco install -yes filezilla
choco install -yes sourcecodepro
choco install -yes urlrewrite
choco install -yes skype
choco install -yes inkscape
choco install -yes graphviz
choco install -yes ghostscript.app
choco install -yes sqlite.shell
choco install -yes python3

rem This reported (at the very end) that it had failed, apparently because of an
rem inability to assign to the Path variable.  I ran it again with the same
rem result.  However, it appears to be installed, and the expected programs are
rem on the path.
choco install -yes miktex

rem I did this so I could build a project that still uses NUnit, although I'm
rem not actually running the tests.
choco install -yes nunit.install

rem I did this when trying to build a Rust project called mysteryshack.
choco install -yes rust
choco install -yes openssl.light


rem With any luck, you'll never have to launch Windows Media Player.  And you
rem may need some luck: this failed the first time through, with the "404" error
rem mentioned on chocolatey.org.  But it worked the second time.
choco install -yes vlc

rem  Would kind of prefer a CLI, but this is convenient enough.  Actually I have
rem  some PS1 script for this somewhere, but it's also useful to inspect
rem  user/system.
choco install -yes patheditor

rem Only needed for "legacy" projects.  I almost always use it through vc-mode,
rem but I get tortoise because I occasionally prefer its log viewer.
choco install -yes tortoisesvn
