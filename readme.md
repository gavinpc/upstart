# upstart

Every so often it happens: I need to set up a new system. Maybe it's really a
whole new machine, maybe just a new drive. Either way, it's a pain. A
notorious pain. This repository is aimed at reducing that pain, nothing more.

Some people set up systems for a living—thousands of them—and those people do it
The Right Way™, by flashing a drive with a pre-formed system image. Well, I do
something else for a living.

In practice, a lot of these notes are now obsolete, and the availability of
package managers has made installation scripts less of an issue. Mostly now
it's just a way of schlepping around my Emacs init.
