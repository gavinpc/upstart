#!/bin/bash

# Install ImageMagick on a Debian-based system.

set -e

sudo apt install imagemagick

exit 0
