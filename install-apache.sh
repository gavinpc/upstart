#!/bin/sh

# Install Apache and mod_wsgi

sudo apt-get -y install apache2

# On earlier "Ubuntu" releases, it was necessary to install this package
# separately.  Now it's included by default, but still must be explicitly
# enabled.  See http://askubuntu.com/a/809660
# 
#sudo apt-get -y install apache2-mpm-worker
# 
sudo a2dismod mpm_event
sudo a2enmod mpm_worker

# Not sure what's up with this one.  It's also not available on "Xenial", but I
#don't see any specific guidance as with the above.
# 
# sudo apt-get -y install apache2-threaded-dev
cd ~/Downloads
git clone https://github.com/GrahamDumpleton/mod_wsgi.git
cd mod_wsgi
# sudo is needed for a "site-wide" installation
sudo python setup.py install

# Right now this isn't working, and neither is the install-from-source method,
# although the latter gives a more informative message.

# Nope.
# 
#sudo pip3 install mod_wsgi
# 
# No, this was never supposed to work in the first place.

# Then I did
# 
# sudo apt-get install -y python-pip
# 
# Which is for Python 2 family.  Then
# 
# sudo pip install mod_wsgi

# Nope.  Same result.  This was supposed to work, though.



# The install-from-source method

# Nope.
# 
# cd ~/Downloads
# git clone https://github.com/GrahamDumpleton/mod_wsgi.git
# cd mod_wsgi
# 
# sudo is needed for a "site-wide" installation
sudo python setup.py install

# I'm supposedly missing something called apxs (maybe apxs2).
#
# There is no package called apxs or apxs2
# 
# I've tried installing httpd-devel
# I've tried installing python-dev

# Maybe the problem is that I have to get the threaded-dev thing working or
# enabled.
# 
# https://ubuntuforums.org/showthread.php?t=1425646

# I installed
sudo apt-get install -y apache2-dev
# and that changed the error: now both commands say that I'm missing something
# called `setuptools`.  Is that for Python or for the system?  Anyway, I'm
# pretty sure I also installed that last night.
# 
# I don't know, but I ran
sudo pip install setuptools

# And then the setup command worked (in mod_wsgi working directory).
sudo python setup.py install
