#!/bin/sh

# Get and build Tup
sudo apt-get -y install libfuse-dev
cd /opt
# Need sudo on the following two because this is under /opt
sudo git clone git://github.com/gittup/tup.git
cd tup
sudo ./bootstrap.sh
man ./tup.1
