#!/bin/sh

# Google Chrome is considered a "non-free" browser, and rightly so, despite what
# they say.  At any rate, it's an essential part of testing web programs,
# especially if you're doing remote debugging (although supposedly Firefox
# offers that now).

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
# Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).

sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

sudo apt-get update
sudo apt-get install google-chrome-stable

# Courtesy of http://ubuntuforums.org/showthread.php?t=1351541 and
# http://tecadmin.net/install-google-chrome-in-ubuntu/

# Gratuitous Chrome irritant:
# https://productforums.google.com/forum/#!topic/chrome/xTKRGDi6Rb4
