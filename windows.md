# Windows

## install

I always do a "new install."  As of Windows 7, the only special setting is to
make sure that you create a "boot" partition (for Windows and installed
programs) no larger than you expect to need.

Also, although I always use a dedicated "home" partition separate from the OS,
don't create it during the installer.  You can't (that I know of) control which
partition it's going to choose to put Windows on.


## home drive

1. Create NTFS partition for home drive, using Computer Management > Storage >
   Disk Management.

2. Assign it drive `D`.  If that's already assigned to the CD drive, you can
   also change that from Disk Management.

3. Create `home/gavin` directory there.

4. Move your personal folders by going to `C:\Users\gavin` and for each folder
   that you want to move,
   
   1. Properties > Location > Move
   2. Enter a path under `d:\home\gavin` (creating a new folder there)
   3. Say OK about moving the files
   
   I used to do this rigorously for all of them, but in fact the only reason I
   do it at all is in case things get inadvertently saved there, that things get
   backed up.  So I only do it for
   
   - `Desktop`
   - `Downloads`
   - `Documents`
   - `Pictures`
   - `AppData\Roaming` (need to show hidden folders for this, see "settings")

5. Set the `HOME` environment variable for my account to `d:\home\gavin`

## drivers

For most machines—especially laptops—you'll need special drivers.

### the dv7

On this HP, for example, immediately after install, the video resolution is
really low.  It's not recognizing that my resolution can go higher than
1024x768.

HP says that it doesn't have drivers for the
[Pavilion dv7-6c95dx Entertainment Notebook PC](http://support.hp.com/us-en/drivers/selfservice/HP-Pavilion-dv7-Entertainment-Notebook-PC-series/5169332/model/5210873)

And then it has
[the nerve to complain](http://support.hp.com/us-en/product/HP-Pavilion-dv7-Entertainment-Notebook-PC-series/5169332/model/5210873/document/c03785459/)
about the third-party sites providing drivers with their logo.  Like
[this one](http://www.helpjet.net/files-HP-Pavilion-dv7-6c95dx.html), which just
got my wi-fi working.

Once you have the network working, you should be able to "bootstrap" (i.e. get
the drivers from the machine itself).
  
They're all copied to `d:\home\gavin\local\dv7-drivers`.  I've included some as
"skipped" so that I could mark what they are, because I see now that the
categories on the `helpjet` site contain not just successive versions of the
same driver, but actually entirely different things in some cases.  But since
they're not labeled on the site, I can't tell exactly what they are until I
start the installer.

I had to install the USB 3.0 drivers twice—either I did something wrong the
first time, or hadn't installed one of the prerequisites yet.

## features

The `choco` install of `urlrewrite` failed.  I suspect this is because I didn't
have IIS turned on.  So really, this should go first.

Go to "Programs and Features" then Turn on or off Windows features.

I turned *off* Internet Explorer 8 (!).

I turned *on* IIS and enabled most of the sub-options.

I turned *on* "Telnet Client" as described
[on ss64](http://ss64.com/nt/telnet.html), when the MSYS `telnet` wasn't working
as I expected.  The "Microsoft Telnet" client is even worse.  Either way, I
assume the problem is my fault.


## settings

NOTE: There are a number of new settings for Windows 10 that I'd set as a matter
of course, but that I've so far only had to deal with once.  The main one
(that's hard to find, anyway), is opening Explorer to "This PC", which is in
fact under folder options, but easy to miss, in a dropdown at the top of the
dialog.  Most everything else is about setting sane privacy settings, which are
not the defaults.

### Services

I used to disable services rather agressively.  I don't have handy the list of
services that I found I could disable in Windows 7, and I don't know how much of
it would translate to Windows 10, anyway.  But I do know that in Windows 10, the
only way to (hopefully) turn off automatic updates (and
avoid
[ridiculous behavior from `TIWorker.exe`](http://tunecomp.net/tiworker-exe-cpu-usage-reaches-50-or-100-in-windows-10-fix-tiworker-issue/),
not to mention the uninterruptible *hours* that Windows Update will arbitrary
force you spend) is to disable the services:

- Windows Update
- Windows Modules Installer

Other changes:

- Program Compatibility Assistant: Automatic -> Manual
- Certificate Propagation: Automatic -> Manual
- [Connected User Experiences and Telemetry](http://www.ghacks.net/2015/11/19/microsoft-rena-and-telemetry/): Automatic -> Disabled
- Skype Updater: Automatic -> Disabled (This is in addition to turning off
  automatic updates in Skype's settings)

### System time (for dual boot)

If you're dual-booting Windows and Linux, you'll run into a problem where the
clock is thrown off by several hours each time you switch systems.  This is
because Windows treats the hardware clock as local by default, while Linux
treats it as universal, as explained in the
["Ubuntu Time Management"](https://help.ubuntu.com/community/UbuntuTime#Multiple_Boot_Systems_Time_Conflicts),
which provides the following registry adjustment to make Windows use UTC:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation]
     "RealTimeIsUniversal"=dword:00000001
```

The file is in this repository as ` WindowsTimeFixUTC.reg`.  Run it (as
Administrator) to apply the settings.

To prevent Windows from changing the clock on *shutdown*, you also need to run
the following (as Administrator).

```
#!bat
sc config w32time start= disabled
```

	 
### Firewall

You need to take this step to enable accessing the web server from other
machines on the LAN.

http://stackoverflow.com/a/4597781/4525

Check the box by the rule in the confirmation step even though it says the rule
already exists.

### Problem reporting

I have a whole essay on this.  The problem reporting "feature" can cause major
problems when using the C# compiler (`csc.exe`).  When the latter exits with an
error status, a process called `werfault` locks out the build target for a long
period, and after investigating ways to completely kill werfault, I found there
was no silver bullet.  The *least* that you can do is administratively

1. Start
2. "Choose how I report problems" / check for solutions
3. Never

For Windows 10, the screenshots given
in [this post](http://superuser.com/a/951050) are already out of date.  I
disabled the "Windows Error Reporting" service, but I found later that
`werfault.exe` was still launching (and causing problems), and that the service
had been re-enabled.  This presumably happened during a windows update, which
reset some other settings I'd made.  Setting the registry key documented in the
above post (included here as `WindowsDisableErrorReporting.reg`) does seem to
have worked, though.

### Explorer view settings:

- Show hidden files.
- Don't hide extensions
- Don't hide system files
- Don't hide empty drives

### Command shell settings:

- Quick edit mode (what allows you to select text).
- **TODO**:  enable true type fonts and set Consolas 24
http://www.howtogeek.com/howto/windows-vista/stupid-geek-tricks-enable-more-fonts-for-the-windows-command-prompt/

### Sounds

Turn off system sounds

- Start > Sound > Sounds > Scheme "No sounds"
- **Don't forget** to uncheck "Play system startup sound"!

### windows update

Turn off automatic updates

It'll keep bothering you until you find a way to silence it.

### Taskbar settings

- auto hide
- combine buttons when full
- notifications
    - windows explorer: show icon and notifications
    - action center: *hide* icon and notifications
- "pinned" programs

### Desktop icons

none (remove Recycle Bin)

### AutoPlay

- use AutoPlay for all media and devices: NO
  
### Start Menu

(under Taskbar and Start Menu)

- power button action: sleep
- store and display recently opened items: no
- store and display recently opened programs: no
- customize:
    - all radio items should be "don't display this item"
    - everything should be unchecked *except*
        - enable context menus and dragging and dropping
            - highlight newly installed programs
            - open submenus when i pause
            - search programs and control panel
            - sort all programs menu by name

### power

I never want automatic timeouts because I don't want some non-resumable process
to be interrupted just because the machine is unattended.  I'll sleep the
machine when I want it to.

Change when the computer sleeps
- Put the computer to sleep: never, never

- When I press the power button: sleep, sleep
- When I close the lid: do nothing, do nothing

### ergonomics

**TBD** Set keyboard repeat rate.  script for this in old repo

Map "Caps Lock" to "Ctrl".  The following registry setting is included in this
repo as =CapsLockToCtrl.reg=.

```
REGEDIT4
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layout]
"Scancode Map"=hex:00,00,00,00,00,00,00,00,02,00,00,00,1d,00,3a,00,00,00,00,00
```

Set the system text size to "Medium."

Trackpads now use "natural" scrolling direction, and on the Mac so does the
mouse wheel by default.  Windows 10 will let you reverse the scrolling
direction, but not per-device.  To get just the mouse wheel consistent with the
others, I found
that
[this Powershell command](https://superuser.com/questions/310681/inverting-direction-of-mouse-scroll-wheel#comment561670_364353) worked.


### Hosts

Normally the easiest way to run web sites locally is to use an alternate port
(e.g. 8000).  But sometimes I need to run local sites on port 80.  To prevent
these from conflicting with each other, I use a dedicated hostname.  At home, my
router is configured (using dnsmasq) to intercept requests for such hostnames.
For this to work on other people's networks, you need to add the hostname to
your host configuration file, which on Windows is
`c:\Windows\System32\drivers\etc\hosts`.  As administrator, add the following
line to the `hosts` file:

    127.0.0.1                     www.whatever.whatever

## software

### bootstrap

Chocolatey has come a long way.  From this point, I was able to install 90% of
what I needed using `choco install`.

So start by getting Chocolatey.  Of course, you have to be online now.

You can install chocolatey itself with the following shell command.  If you run
it as administrator, then Chocolatey's `bin` directory will be added to your
*system* path; otherwise it'll be added to your user path.  Both methods work.
The only difference I've run into is that, since the system path always precedes
the user path, you can't cause user path entries to precede it.  I've moved it
from one to the other after the fact, and it doesn't appear to matter.

```
#!dos
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

I mean, you could go copy it from the
[Chocolately install page](https://chocolatey.org/install), but you'd have to
use Internet Explorer for that!  The file `install-chocolatey.bat` is also
included in this repository, but again, you'd have to get it from bitbucket
somehow.

Note that before you start installing applications (e.g. by running
`install-packages.bat`), you probably want to

```
#!dos
choco upgrade -yes powershell
```

and then reboot.  It's possible that some installers won't work with the “stock”
PowerShell but will work with later versions (see,
e.g. [this comment on ffmpeg's package](https://chocolatey.org/packages/ffmpeg#comment-2714396394)).


### applications

Chocolatey (an CLI installer) was installed during the "bootstrap" step.
Everything else is classified in terms of whether it can or should be installed
with `choco install`.

`choco` commands should be run as Administrator (per their warning).  Without
the `-yes` argument, you're asked to confirm the license agreement at the
begining of (all?) package installs.  Even *with* the `-yes` argument, you're
(now) also required to confirm the installation of any packages that are missing
checksums.  That choice times out after 30 seconds, so unfortunately, you can't
just leave the thing unattended.

Chocolatey occasionally fails to install a package for reasons that, as it tells
you, “licensed” users probably would not have experienced.  This is more of an
irritant than a showstopper, as I've found that such packages have eventually
installed when retried.  It can help to delete the Chocolatey's temporary
content (`C:\Users\gavin\AppData\Local\Temp\Chocolatey\(package)`), as per
[this comment](https://chocolatey.org/packages/tortoisesvn#comment-2139892041)
about TortoiseSVN (which also took me three tries).

This repository includes a script called `install-packages.bat` which will pull
everything that I normally use.

The following applications have auto-update features, which should be turned off
so as not to conflict with `choco`.  It's not clear that choco has a story for
dealing with that.

- Firefox
- Chrome
- Thunderbird
- f.lux now has an auto update that's enabled by default

For .NET framework, you have to add it to your path.

    c:\Windows\Microsoft.NET\Framework\v4.0.30319

I do *not* use the 64-bit framework tools.  It shouldn't make any difference for
what I'm doing, but for some reason I have problems with `csc.exe` crashing
(especially under Tup), which isn't a problem with the 32-bit one.

Stuff to install manually

- SQL Server: (too big -- need interactive installer) 2016 apparently doesn't
  support Windows 7, so using
  [SQL Server 2014](https://www.microsoft.com/en-us/download/details.aspx?id=42299).
- Reactor: my repo, which also requires a service install.
- Tup: (just binaries for Windows)

I have a start on a Powershell script that downloads and installs Tup, but it's
not worth finishing because it takes all of one minute to do it.  Just download
http://gittup.org/tup/win32/tup-latest.zip, extract it to `c:\opt\tup`, and add
that folder to your `PATH`.

Stuff that I tried but didn't keep:

- MinGW (`mingw`) (I uninstalled this and had to manually remove its `PATH`
  entry.  I also notice that there's still a folder `c:\tools\mingw64` with all
  the content.)


Stuff that I sometimes used but could get going without

- Firefox Dev
- Chrome Canary
- GIMP
- Fiddler (actually I kind of hate Fiddler now)

To install node packages, use `install-node-packages.sh`.  It's assumed that by
this point, you have `bash`.

### software settings

#### Emacs

Update 2017-05-20.  I gave up on Chocolatey for Emacs.  (Which is becoming a
pattern.)  [The emacs w64](http://emacsbinw64.sourceforge.net/) project released
binaries for 25.2 about three weeks ago, but they aren't on Chocolatey yet.
Meanwhile, Emacs has been crashing a lot.  I don't know if it's the build, or
Windows 10, or this machine (with, e.g., high-dpi).  But I thought it would be
worth trying 25.2 I unpacked it to `c:/opt/emacs`, put `c:/opt/emacs/bin` on the
path, and re-ran `shell.bat`.  So far, everything appears to be working as
before (though all I've done is type this).

Update 2017-06-05.  This is definitely better.  I don't think there have been
any crashes since switching to the new binaries.

Update 2017-08-27.  This is still stable.  I haven't noticed any crashing.
However, the redisplay is dreadfully slow.  I think this was the case before,
and I was just too distracted by the crashing to notice it.  I'm assuming it has
to do with hi-dpi, as it doesn't occur on other displays.

I also modify `emacs.exe` to run with DPI scaling disabled (which is an option
in the Compatibility tab).  I only noticed this because Emacs incorrectly
reports the pixel size of my external display, which is not HI-DPI, though it
correctly reports my main display, which is.



#### MSYS2


distributed with the `git` package.  I went back to `msys` when I was having
**NOTE**: See note on `git`.  I was originally using the "msys" tools that are
problems with Tup that may or may not have been related.  But even if the Tup
issue was unrelated to the Git msys versus this one, I think it makes sense
having a dedicated msys apart from Git.

To install MSYS, I went through [this process](https://msys2.github.io/).  I
have no idea what was going on and I wasn't left with great confidence that I
could repeat the performance.

Note also that you need to put `c:\msys64\usr\bin` in your *system* path in
front of `C:\Windows\System32`, otherwise the (useless) `FIND.EXE` that comes
with Windows will mask it.  This might break some stuff (in particular I think
Cordova build uses it), but too bad.

MSYS includes a “minimal” set of packages initially.  Once you've gone through
the above, you can add packages using `pacman`.  I've done the following:

```
#!sh
pacman -S patch
pacman -S diffutils
```

Diffutils includes the `diff` command, which is required for certain emacs
features to work completely.

I installed `make` at some point and then realized I didn't need it.

#### Git

Before it lets you make any commits, you have to configure Git with a username
and email.  Because I edit everything in Unix-based editors, I also don't want
any automatic CRLF conversion.

```sh
git config --global user.name 'Gavin Cannizzaro'
git config --global user.email 'whatever'
git config --global core.autocrlf false
```


#### Python

On Unix-like systems, [PEP 394](https://www.python.org/dev/peps/pep-0394/)
recommends that you use a command called `python3` when Python 3.x is intended.
Scripts that observe that convention won't work on Windows with the current
`python3` package from Chocolatey, which doesn't provide a `python3` command.

As administrator, I added a symlink as follows:

```
ln %PROGRAMDATA%/chocolatey/bin/python.exe %PROGRAMDATA%/chocolatey/bin/python3.exe
```

This directory is managed by Chocolatey, so it's not a great solution.  But
something like it has been
[mentioned in the package discussion](https://chocolatey.org/packages/python3#comment-2471628132),
so it may get added in a future version.

**UPDATE**: The latest `python3` package is 3.6, which installs to `C:\Python36`
and adds that folder (as well as its `bin`) to the system `PATH`.  So I use the
following link:

```
ln C:/Python36/python.exe %PROGRAMDATA%/chocolatey/bin/python3.exe
```


#### SQL Server

- change the default database location to `%HOME%\db`
- change the "SQL Server" service to log on with "Local System", which avoids
  the "Access Denied" problem that you get when trying to attach to a database.
  See
  [this issue](https://connect.microsoft.com/SQLServer/feedback/details/539703/access-denied-attaching-a-database-when-permissions-are-inherited).
  Adding explicit permissions to those files for my account did not fix it for
  me, but the service logon change did.  I'm sure it has some other
  implications, but I'm only using the instance locally for development.


#### Process Explorer

- Replace Task manager: yes
- Run at logon: yes
- Hide when minimized: yes
- Allow only one instance: yes
- graph background color: black

#### Firefox

- when Firefox starts: show a blank page.
- download files to desktop
- "use custom settings" for history
- never accept 3rd party cookies
- delete cookies when I quit firefox
- don't accept cookies (the above settings are for when you have to temporarily
  re-enable them)
    - make exceptions for selected https sites
- security / remember logins for sites: NO
- advanced / update / never check for updates (so... now I have to run `choco
  upgrade firefox` every six weeks?)

#### Google Chrome

You need to open "Advanced settings" to see most of these.

- On startup: Open a specific set of pages:
  - about:blank
- Use a web service to help resolve navigation errors: NO
- Use a prediction service to help complete searches and URLs typed in the
  address bar: NO
- Use a prediction service to load pages more quickly: NO
- Send a "Do Not Track" request with your browsing traffic: YES
- Offer to save your web passwords: NO
- Privacy / Content Settings:
    - Keep local data only until you quit your browser
    - Block third-party cookies and site data
  
The "keep local data" setting will require you to whitelist sites where you want
to stay logged in.  For most sites, it's only necessary to "Allow" one domain,
but some require more.  To use the Azure portal, for example, you need to allow
the following three domains:

- `portal.azure.com`
- `login.live.com`
- `login.microsoft.com`



#### Skype

- change status to away after 4 minutes idle
- privacy:
    - accept Skype browser cookies: NO (and clear them while you're at it)
    - Allow Microsoft targeted ads, including use of Skype profile age and gender: NO!
- sounds
    - i sign in: no
    - i sign out: no
- notifications
    - notify me when someone in my contacts goes online: NO
- advanced
    - automatic updates: turn off automatic updates
    - keep skype in the taskbar while I'm signed in: NO

#### Thunderbird

I copied the entire "Thunderbird" directory from my old `AppData`, replacing
anything that was there (I think it was empty), and that put things just as I'd
left them.

#### Filezilla

I copied the entire "Filezilla" directory from my old `AppData`, replacing
anything that was there (I think it was empty), and that put things just as I'd
left them—except that some certificates needed to be re-approved upon
connection.

#### Sumatra PDF

- automatically check for update: NO
- Make SumatraPDF my default PDF viewer

#### Inkscape

This is not for Inkscape *per se*, but I came across it when trying to use
Inkscape to open an EPS file.

On Linux, Inkscape has “out of the box” support for EPS, but on Windows,
[additional steps are needed](http://clownfishcafe.blogspot.de/2014/05/importing-eps-files-into-inkscape.html).

The above steps are easy to follow and work as described.  I just installed
`ghostscript.app` via Chocolatey, and added its `bin` and `lib` to the path.
The next time I started Inkscape, EPS was a recognized type!

#### Graphviz

The Chocolatey installer did not add this to the path (for
[better or worse](https://chocolatey.org/packages/Graphviz#comment-2003124681)),
so I manually added

    %PROGRAMFILES(x86)%\Graphviz2.38\bin

which is the version I happened to get.  I would rather that the package itself
would do this.

#### VLC player

On startup, you're offered two checkboxes about what network access the app is
permitted.  I *unchecked* both of them, because I don't need them now.

## Data

At this point, you should be ready to get things from your archives.

This includes pulling repositories.

Copying files from backup drives, many of which have designated locations.

## backup

One-time backup-related configuration:

1. create `d:\mount\posthumus`

2. in Computer Management / Storage / Disk Management, choose “Change Drive
   Letter and Paths” from the target drive's context menu.

3. Choose “Add”, then under “Mount in the following empty NTFS folder” enter the
   above path.

Then you should be able to run `backup-home-to-posthumus.bat`.  I always put
these under `d:\home` and I haven't tested them from anywhere else.


## miscellaneous

Some things are only needed when certain conditions arise.

### turn on fusion logging

When you get an assembly binding failure, you need to set these settings to
debug it.

[Here is a summary of the instructions for turning on "fusion" logging](http://stackoverflow.com/a/1527249/4525).

```
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Fusion
Add:
DWORD ForceLog set value to 1
DWORD LogFailures set value to 1
DWORD LogResourceBinds set value to 1
DWORD EnableLog set value to 1
String LogPath set value to folder for logs (e.g. C:\FusionLog\)
```

> Make sure you *include the backslash* after the folder name and that the
> *Folder exists*.
> 
> You need to restart the program that you're running to force it to read those
> registry settings.
>
>  BTW, don't forget to turn off fusion logging off when not needed.

You will also probably need to turn it off after you're done, as this will break
Tup-based builds (it detects the writes to the fusion log).

### SSMS with high DPI displays.

As of version 16.3, SSMS is high-DPI "aware," so it's usable.  But it still uses
bitmap scaling by default, so the text is not sharp.  I
followed
[these instructions](https://blogs.msdn.microsoft.com/sqlreleaseservices/ssms-highdpi-support/) to
enable high-DPI mode, and it worked.  I did not need to restart the machine,
even though the `PreferExternalManifest` flag had not been previously set.


### using SSMS as a SQLite client

Not sure what this falls under.  But you can apparently use SSMS as a SQLite
client, by
[setting up a data file as a linked server](http://superuser.com/a/395290).
[This post](https://www.mssqltips.com/sqlservertip/3087/creating-a-sql-server-linked-server-to-sqlite-to-import-data/)
details the whole process.

```
#!sql
EXEC sp_addlinkedserver
	@server = 'test_places',
	@srvproduct = '',
	@provider = 'MSDASQL',
	@datasrc = 'test_sqlite3'
```

I got a [SQLite ODBC driver from here](http://www.ch-werner.de/sqliteodbc/).  I
installed both the 32 and 64-bit drivers.  I don't know whether both are
necessary.

It works, although it appears that the connection doesn't include schema
information.  So you can't see table columns in SSMS's object explorer, and you
do have to use `OPENQUERY` (or `OPENROWSET` or whatever) instead of the
“four-part-name” references.

### playing back 96kHz audio in Cubase

If you're using Cubase without a special audio interface, then you may not be
able to play back audio higher than 48kHz using Steinberg's "generic" ASIO
driver.  Fortunately, I found that installing the driver
from [ASIO4ALL](http://asio4all.com/) worked immediately and perfectly at 96kHz,
and no, it's [not a virus](https://forum.ableton.com/viewtopic.php?f=1&t=96894).

### Launchy on hi DPI screen

This kind of worked

https://sourceforge.net/p/launchy/discussion/451016/thread/0458bc4d/

https://sourceforge.net/p/launchy/discussion/451016/thread/0458bc4d/bc5c/attachment/Black%20Glass%20HiDPI.7z

### Okular

Looking for a free PDF viewer with highlighting, I saw
[this](http://douglaswhitaker.com/2014/06/pdf-reader-for-windows-with-highlighting/) and
wanted [Okular](https://okular.kde.org/).  It's KDE-based, and
[doesn't have pre-built binaries for Windows](https://okular.kde.org/download.php).
It does
[seem like](http://stackoverflow.com/questions/39566803/is-it-possible-to-install-simply-okular-for-windows)
you have to do the
[KDE platform installer](https://techbase.kde.org/Projects/KDE_on_Windows/Installation) 
first, which I installed.  But I wasn't willing to figure out how to build it from
source, considering that the platform-package approach is
[deprecated in favor of application-specific installers](http://kfunk.org/2016/06/18/kde-on-windows-update/).
But [Okular doesn't have one](https://community.kde.org/Windows)...
[until now](https://sourceforge.net/projects/okularinst4win/).

http://tuxcolombia.blogspot.com/

http://tuxcolombia.blogspot.com/2012/03/simple-installer-for-okular-in-windows.html

I installed it, and it works okay as a viewer, but it crashed after not very
long.

But I've used it for a bit since, and it hasn't crashed again, and the
highlighting works.

### Android remote debug over USB

For Samsung tablet,

http://developer.samsung.com/galaxy/others/android-usb-driver-for-windows

It says mobile phone, but it still works for tablet.
