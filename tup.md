# Tup

[Tup](http://gittup.org/tup/) is a build system that changed my world.

Like many programs, it has a "PPA" option that you can use to install
on Debian-based systems:

```
#!sh
sudo apt-add-repository 'deb http://ppa.launchpad.net/anatol/tup/ubuntu precise main'
sudo apt-get update
sudo apt-get install tup
```

I've tried that, but I found that it didn't work at all.  Besides, Tup is of
course easy to build, and it's useful to be able to test modifications to it.

The following script is in this repository as `install-tup.sh`.  Note
that
[you first need to install the FUSE library](http://stackoverflow.com/a/16379576).

```
#!sh
# Get and build Tup
sudo apt-get -y install libfuse-dev
cd /opt                         # gpc
# Need sudo on the following two because this is under /opt
sudo git clone git://github.com/gittup/tup.git
cd tup
sudo ./bootstrap.sh
man ./tup.1
```

Note that Tup's build depends on `git`.  Since you run the above (bootstrap)
build as root, you have to have `git` in the `PATH` for root.

Of course, you have to have Git installed.  And to use tup from anywhere, you
need to add it to the path.  For that, you have to add it to your
`PATH`
[in your shell profile](http://linuxconfig.org/permanently-add-a-directory-to-shell-path).

```
#!sh
PATH=$PATH:/opt/tup
```
