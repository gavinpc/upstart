#!/bin/sh

# I use keyboard repeat for navigation, and faster is better.  In Linux GUI
# systems, you can control the keyboard repeat rate through your "X" settings.
# See Bob Scheifler, et al, "xset" man page.
# http://www.x.org/archive/X11R7.5/doc/man/man1/xset.1.html

# Set keyboard repeat delay and rate
# xset r rate 300 50              # ms delay, repeats per second

# Supersedes above, see https://askubuntu.com/a/848513

gsettings set org.gnome.desktop.peripherals.keyboard delay 300
gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 11
