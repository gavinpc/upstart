@ECHO OFF

REM Need to explicitly move to this directory because shortcuts run as
REM administrator disregard "start in" folder and start in system32.
PUSHD D:\home
CALL backup-home-to-mount posthumus
POPD
