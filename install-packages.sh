#!/bin/sh

# General use
sudo apt-get install -y mercurial
. ./install-flux.sh

# Everything else is needed for willshake.

#--- To clone it
sudo apt-get install -y git

#--- To build it
sudo apt-get install -y xsltproc
sudo apt-get install -y graphviz
sudo apt-get install -y imagemagick
sudo apt-get install -y inkscape
sudo apt-get install -y python3-pip

# Which see
. ./install-ffmpeg.sh
. ./install-tex.sh
. ./install-chrome.sh

# I included the -H per pip3's recommendation.  This doesn't work without sudo,
# although it doesn't indicate any error.
sudo -H pip3 install pygments

#--- To run it

# Includes mod_wsgi
./install-apache.sh

# This is optional.
npm install -g livereloadx
