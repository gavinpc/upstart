#!/bin/bash

# Install Graphviz on a Debian-based system.

set -e

sudo apt install graphviz

exit 0
