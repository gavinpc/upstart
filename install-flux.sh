#!/bin/sh

# F.lux was created by Michael and Lorna Herf (or at least it's copyrighted to
# them).  The GUI was made by Kilian Valkhof.

# UPDATE: it has randomly stopped working on Linux.  I haven't tried
# reinstalling it.

# Formerly at ppa:kilian/f.lux
sudo add-apt-repository -y ppa:nathan-renniewaldock/flux
sudo apt-get update
sudo apt-get -y install fluxgui
