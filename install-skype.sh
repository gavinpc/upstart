#!/bin/bash

# Install Skype using snap.

set -e

# https://linuxize.com/post/how-to-install-skype-on-ubuntu-20-04/
sudo snap install skype --classic

exit 0
