# OSX (a.k.a. "mac OS")

So. I use a Mac now.

# system settings

Set default browser to +Chrome+ Firefox. You have
to [get Firefox](https://www.mozilla.org/en-US/firefox/) first.

## disable Siri

Uncheck "Enable Siri" in System Preferences > Siri

## text size

Most of my font settings are done in Emacs. For Terminal, you can just go into
Preferences and increase the font size of the default profile.

## configure keyboard

### Modifier keys

As elsewhere, map Caps lock to Control.

- System Preferences > Keyboard > Modifier Keys > Caps Lock Key -> Control

Also, the `fn` key is where `Ctrl` should be, as noted
in [this post](https://apple.stackexchange.com/a/88905). However, that
is [broken on Sierra](https://pqrs.org/osx/karabiner/), the latest version of
mac OS. I haven't followed up on this
"[temporary solution](http://slongwell.github.io/articles/2016-09/karabiner-workaround)"
because you can get around it in Emacs using `mac-function-modifier`, which is
good enough for me so far.

For a “Windows” keyboard, you also have to map “Option” to “Command,” or you
lose the Alt+Tab that works everywhere else. As a result, in the Emacs config,
I have to set `(setq mac-command-modifier 'meta)`. The annoying thing about
this is that you have to use “Alt” instead of “Ctrl” when outside of Emacs. But
at least you're not on a Mac keyboard.

Also change the "Move focus to next window"

https://apple.stackexchange.com/a/50759

This allows me to use it for `switch-to-buffer` in Emacs.

### Kill Siri key

Just applied the solution in ["How to get macOS Sierra to stop asking me to
enable Siri"](https://apple.stackexchange.com/a/283035), and we'll see how it
goes. I have hit this several times a day.

> Go to:  → System Preferences → Keyboard, then click the "Customize Control
> Strip" button on the bottom.

### Repeat rate

Set a faster rate using [this method](https://apple.stackexchange.com/a/83923)

```
#!sh
defaults write -g InitialKeyRepeat -int 13 # normal minimum is 15 (225 ms)
defaults write -g KeyRepeat -int 1 # normal minimum is 2 (30 ms)
```

### Shortcuts

In Keyboard > Shortcuts > Spotlight, uncheck (or remap) “Show Finder search
window” so that `mark-sexp` works in Emacs.

## switching spaces

Full-screen windows are in their own "space," which is of dubious value, since
you can't put another window in front of a full-screen window. Worse, the
switching back and forth is dizzying due to the animation.

You can disable this by checking

- System Preferences > Accessibility > Display > Reduce Motion

It's not any faster, that I can tell, since it replaces the slide with a fade.
But it's much less dissonant.

The following attempts to actually make the transition faster have not worked.

Following
[this article](http://osxdaily.com/2015/01/06/make-the-window-resizing-animation-speed-instant-in-mac-os-x/),
I ran

```
#!sh
defaults write -g NSWindowResizeTime -float 0.003
```

Doesn't seem to be working.

Neither does the Info.plist approach
described [here](https://apple.stackexchange.com/a/146951).

# get homebrew

You can get the latest install script from https://brew.sh/

Currently, it's

```
#!sh
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

# get emacs

**UPDATE** (2021-02-08) I have once again gotten an Emacs with XWidgets running
on Mac OS.

I mostly followed the post [A browser in your Emacs
(macOS)](https://jft.home.blog/2020/01/24/a-browser-in-your-emacs-macos/). This
worked except that I had to remove the `--with-jansson` option, which gave

```
Error: invalid option: --with-jansson
```

```
brew tap daviderestivo/emacs-head
brew install emacs-head --HEAD --without-imagemagick@7 --with-cocoa --with-xwidgets
ln -s /usr/local/opt/emacs-head@27/Emacs.app /Applications
```

I'm pretty sure I resorted to this because the following (2017) update didn't
work on this Mac.

**UPDATE**: I'm using someone's fork that has XWidget support. Notes are in
Mindgrub GitLab, `gcannizzaro/notes/emacs.org`.

I'm currently using emacs installed with

    brew install emacs --HEAD --devel --with-cocoa --with-librsvg --with-imagemagick@6

As of this writing (2017-12-09), this gives me emacs version 27... and 26 hasn't
even been released yet. However, so far so good.

I also followed the first half
of [these instructions](https://korewanetadesu.com/emacs-on-os-x.html), putting
"Emacs Server" and "Emacs Client" scripts under `/Applications/Development`.
This mostly does what I want, but I should probably follow the rest of them,
because for example if I open a file through the shell, I end up with what looks
like another instance inside of a "tabbed" container, which isn't quite what I
want.

Also, since I am using a homebrew install, I changed the script to the
following:

```
#!AppleScript
tell application "Terminal"
	try
		# do shell script "/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -c -n &"
		do shell script "/usr/local/bin/emacsclient -c -n &"
		tell application "Emacs" to activate
	on error
		do shell script "/usr/local/bin/emacs"
	end try
end tell
```

This way, I can use "emacs client" from spotlight and get the same thing as I'd
get from the CLI.

### Emacs CLI

**UPDATE** But see above.

New Mac, Catalina, a lot is different from above, still need to update. In
short:

- can't seem to build from homebrew
- running the “stock” emacsforosx build
  - which still doesn't support SVG
- you have to do some wierd thing to run the “unsigned/unrecognized” application
- you also have to enable of things for Emacs in “Privacy & Security” settings
  - and I still can't access Desktop from Emacs

One thing I ran into was that the install doesn't put an emacs binary in path.
My Harvest integration complained that `emacs` command was not found. I got
around that this way,

```sh
sudo ln -s /Applications/Emacs.app/Contents/MacOS/Emacs /usr/local/bin/emacs
```

That puts the GUI in path, but if you call with `-nw` (or eval, etc) you get the
terminal version.

Though something else is wrong because the script itself isn't working right...

## backstory

There are two common ways to install emacs on OSX: using a DMG (like most users
install most apps), or using a package manager (homebrew).

As usual, I can't live with the "stock" emacs. On OSX, the closest thing to
that would be [Emacs for Mac OSX](https://emacsformacosx.com/), which I
initially used.

The main problem with this version was its lack of SVG support. I tried various
`brew` configurations. There's a [big discussion of this on
Homebrew](https://github.com/Homebrew/homebrew-core/pull/3330) which seems to
have resulted in no action. Bottom line, most of the configurations there are
now out of date. The above works.

# Mercurial

```
#!sh
brew install hg
```

# gpg

I haven't used GPG directly, but it integrates with Emacs (for files with a
`.gpg` extension)

```
#!sh
brew install gpg
```

However, this isn't working. I
tried
[this](https://unix.stackexchange.com/questions/55638/can-emacs-use-gpg-agent-in-a-terminal-at-all/278875#278875).
There's info [here](https://www.emacswiki.org/emacs/EasyPG) about it, but I
haven't been able to follow up everything.

# `PATH` issue with some applications

"This is an OSX annoying environment issue," as
described [here](https://emacs.stackexchange.com/q/10722) and various other
places.

[This approach](https://gist.github.com/mcandre/7235205) didn't work, probably
because "support for `/etc/launchd.conf` was removed in 10.10."

I ended up going with the `exec-path-from-shell` package. This is an
Emacs-specific solution, but that's the only place it's come up.

# Git global ignore

Following
[this advice](https://help.github.com/articles/ignoring-files/#create-a-global-gitignore),
I created a global git ignore file containing

```
.DS_Store
```

I've never had to do it before, but this is so annoying.

# Git credentials helper

Certain remotes will prompt you all the time for your password. To avoid this,
you can use the OSX Keychain as a provider.

```sh
git config --global credential.helper osxkeychain
```

# Firefox true fullscreen

On Mac, Firefox's “fullscreen mode” isn't. (This appears to be true of Chrome
as well, actually.)

In `about:config`:

```
browser.fullscreen.autohide	true
permissions.fullscreen.allowed	true
```

These were both false. I set both at once and got what I wanted, so I don't
know for sure that both are needed.

# Environment variables

Environment variables set in `~/.profile` or `~/.bash_profile` are not passed to
GUI applications, since they are (apparently) not launched by one of those
shells. You can edit `/etc/launchd.conf` to add an environment setting that
will be picked up by all applications, as in

```
setenv SOME_VAR "some value"
```

Those settings take effect on system start, so a reboot is needed for them to
take effect. As noted [here](https://stackoverflow.com/a/3756686), it is
possible to make such settings take effect immediately using `launchctl`:

```
#!sh
launchctl setenv SOME_VAR "some value"
```

though to make the setting persist, you still need to make the equivalent change
in `/etc/launch.conf`.

**UPDATE**: After having Active Directory installed, the `/etc/launch.conf`
approach no longer works. See notes elsewhere.

# Screenshot location

By default, screenshots go to the desktop, which eventually clutters it.

```
#!sh
mkdir -p /Users/GCannizzaro/screenshots/
defaults write com.apple.screencapture location /Users/GCannizzaro/screenshots/
```

### Raptor RDF utility

**UPDATE** This was not good.

Just came across [Raptor](http://librdf.org/raptor/), which supposedly can be
used to read various RDF syntaxes and write to Graphviz. It's on
[Homebrew](https://formulae.brew.sh/formula/raptor). I installed with

```sh
brew install raptor
```

And now `rapper` is in the path.

### Close terminal window when shell is done

https://stackoverflow.com/a/17910412

## maybe try this

https://blog.jim-nielsen.com/2022/inspecting-web-views-in-macos/

https://news.ycombinator.com/item?id=30648424
