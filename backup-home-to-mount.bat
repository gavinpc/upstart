@ECHO OFF
REM This is a variation on backup-home that targets an NTFS mount point.  Has to
REM be run as administrator for the kill processes to work.

SET log_dir="d:\backup-logs"
SET target_volume=%1
SET target_base=d:\mount\%target_volume%
SET target_dir=%target_base%\%COMPUTERNAME%-robocopy\home

IF "%target_volume%"=="" (
   ECHO usage: %~f0 volume.
   PAUSE
   EXIT /B 1
)

REM The presence of a readme.txt file is used as a proxy for whether the drive
REM is actually there, as I don't know of a formal way to do it.
IF NOT EXIST %target_base%\readme.txt (
   ECHO abort: the volume %target_volume% is not mounted.
   PAUSE
   EXIT /B 1
)

REM Close processes that lock user files
REM
REM This doesn't always work, though, so I usually close it manually.
ECHO Stopping mail client...
taskkill /IM thunderbird.exe

REM Haven't used dropbox in years
REM ECHO Stopping dropbox client...
REM taskkill /IM dropbox.exe

REM Skype also has a number of locks.  Should just avoid the lock files, I guess.

ECHO Stopping SQL Server services...
NET STOP MSSQL$SQLEXPRESS

IF NOT EXIST "%target_dir%" MKDIR "%target_dir%"
IF NOT EXIST "%log_dir%" MKDIR "%log_dir%"

REM This is not a 24-hour clock, and depends on locale, etc.  Good enough for
REM this.  The timestamp will also be in the log itself.
SET timestamp=%DATE:~10,4%-%DATE:~4,2%-%DATE:~7,2%-%TIME:~0,2%-%TIME:~3,2%-%TIME:~6,2%
SET log=%log_dir%\robocopy-home-%target_volume%-%timestamp%.log

ROBOCOPY d:\home "%target_dir%" ^
    /MIR /Z /XO /XJ /NP /R:0 ^
    /LOG:%log% ^
    /XD cache .cache

IF ERRORLEVEL 1 (
   START %log%
   EXIT /B
)
