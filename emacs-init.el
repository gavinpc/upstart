;; ** Emacs server **

;; In principle, I can't tell 100% from here whether this instance is intended
;; to be the server.  But making the caller deal with that is much more
;; complicated, and impossible to do uniformly across platforms.
(require 'server)
(and (display-graphic-p)
	 (not (server-running-p))
	 (server-start))
(setq server-raise-frame t)

(setq mac-command-modifier 'meta)
(setq mac-function-modifier 'control)
(setq mac-option-modifier 'control)

;; Hide chrome
(menu-bar-mode -1)			   ; on mac os this doesn't recover any space anyway
(setq inhibit-startup-screen t)
(setq inhibit-startup-echo-area-message t)

;; Seeing if this helps with some apparently-font-related hangs and slowdowns in
;; recent use on Windows.
(setq inhibit-compacting-font-caches t)

;; ** Ensure packages **

(require 'package)

(setq gpc/packages
      '(
        ;; At some point I really needed bleeding-edge org-mode.  Probably
        ;; unnecessary now that up-to-date emacs builds are available through
        ;; package managers for all platforms.
        ;;
        ;; In fact, I am back to using bleeding-edge org-mode, which is now at 9
        ;; (and has some breaking changes from 8).  But since org is already
        ;; built in, and since it's available in various places, including it
        ;; here is not an effective way to ensure that you're using the latest
        ;; version.  Instead, I just install it from  `list-packages'.
        ;; org
	;; This is failing to install, saying unavailable
        ;; ob-shell			; see below
		;; After a few week's usage, I've found that `jscheid's `prettier' is
		;; better than the more “official” package (`prettier-js'): it's faster,
		;; it does a better job of preserving point position, and it has a
		;; global configuration that avoids the need for a bunch of mode hooks.
		;; https://github.com/jscheid/prettier.el
        add-node-modules-path
        auto-save-buffers-enhanced
        company
        diff-hl
        dimmer
        docker
        docker-compose-mode
        dockerfile-mode
        gitlab-ci-mode
        goto-last-change
        graphviz-dot-mode
        htmlize
        js2-mode
        key-chord
        lua-mode
        magit
        markdown-mode
        multiple-cursors
        org-bullets
        password-generator
        php-mode
        prettier
        rjsx-mode
        smart-mode-line
        smex
        sparql-mode
        stylus-mode
        tide
	;; unavailable for some reason, resolve later
        ;; ttl-mode
	;; still needed?
        web-mode
        yaml-mode
        yasnippet
        ))

;; I mean, it wouldn't *hurt* to have these on other systems.
(cond
 ((eq system-type 'windows-nt) (nconc gpc/packages '(powershell csharp-mode)))
 ((eq system-type 'darwin) (nconc gpc/packages '(exec-path-from-shell)))
 )

;; From the prelude guy, not sure what this does actually.
;;(setq url-http-attempt-keepalives nil)

;; 	;; ("marmalade" . "https://marmalade-repo.org/packages/")

;; Using stable-melpa because of an issue with `prettier'
;; vs https://melpa.org/packages/
;; May be resolved now
;; https://github.com/jscheid/prettier.el/issues/90
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives
;;              '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("org" . "https://orgmode.org/elpa/") t)

(defun gpc/ensure-packages (packages)
  (require 'cl-lib)
  (let ((missing-packages (cl-remove-if #'package-installed-p packages)))
	;; Refreshing the (remote) package archives takes a while, so only
	;; do it if there are missing packages.
	(when missing-packages
      (package-refresh-contents)
      
      (dolist (package missing-packages)
		;; This is a problem if you're offline and you start emacs with
		;; a new package listed.  You can work around it temporarily by
		;; commenting this line.
		(package-install package)
		))))

(package-initialize)
;; Skip this for faster startup.  On new systems, or otherwise when adding
;; packages, you can uncomment or run manually.
;;(gpc/ensure-packages gpc/packages)

;; for Mac
(if (symbol-function 'exec-path-from-shell-initialize)
    (exec-path-from-shell-initialize))	

(global-hl-line-mode 1)  ; highlight current line
(make-variable-buffer-local 'global-hl-line-mode)

;; ** GUI **

;; Adapted from `alpha' package.
(defun transparency-set-value (value)
  "Change the transparency of the current frame"
  (interactive "nPercent transparency: ")
  (let ((clamped (max 0 (min 100 value))))
    (set-frame-parameter nil 'alpha clamped)))

(defun transparency-increase(&optional delta)
  "Increase the transparency of the current frame by DELTA."
  (interactive)
  (transparency-set-value (+ (or (frame-parameter nil 'alpha) 100) (or delta 2))))

(defun transparency-decrease(&optional delta)
  "Decrease the transparency of the current frame by DELTA."
  (interactive)
  (transparency-increase (* -1 (or delta 2))))

(defun gpc/gui-init()
  "Do initialization that only applies in GUI mode."
  
  (tool-bar-mode -1)
  (blink-cursor-mode -1)
  (scroll-bar-mode -1)

  ;;(require 'dimmer)
  ;;(dimmer-activate)

  (transparency-set-value 85)

  (require 'face-remap)
  (setq gpc/variable-face-family "DejaVu Sans")
  ;; https://dejavu-fonts.github.io/
  (setq gpc/fixed-face-family "DejaVu Sans Mono")
  
  (cond
   ((eq system-type 'windows-nt)
	(setq gpc/variable-face-family "Verdana"))
   ((eq system-type 'darwin)
	(setq gpc/fixed-face-family "Courier"))
   ((eq system-type 'gnu/linux)
	(setq gpc/fixed-face-family "Liberation Mono"))
   (t
	;; https://github.com/adobe-fonts/source-code-pro/releases
	(setq gpc/fixed-face-family "Source Code Pro")))

  ;; Type

  ;; We want proportional fonts by default with optional toggling into monospace.
  ;; This is the opposite of what we get by default using `buffer-face-mode', so
  ;; here we reverse the behavior.
  (defun toggle-fixed-pitch ()
	"Call `buffer-face-mode' with the alternate font set to
  `fixed-pitch'.  Note that this sets `buffer-face-mode-face',
  overriding any earlier buffer-local setting."
	(interactive)
	(setq buffer-face-mode-face 'fixed-pitch)
	;; `buffer-face-mode' only toggles when called interactively.
	(call-interactively 'buffer-face-mode))
  (set-face-attribute 'variable-pitch nil :family gpc/variable-face-family)
  (set-face-attribute 'fixed-pitch nil :family gpc/fixed-face-family)
  (set-face-attribute 'default nil :family (face-attribute 'variable-pitch :family))
  ;; Modes where we want monospace.
  (dolist (hook '(calendar-mode-hook
				  profiler-report-mode-hook
				  sql-interactive-mode
				  artist-mode-hook))
	(add-hook hook (lambda () (toggle-fixed-pitch))))


  ;; Colors

  (set-face-attribute 'default nil :background "black" :foreground "NavajoWhite")
  (set-face-attribute 'cursor nil :background "white")
  (set-face-attribute 'region nil :background "skyblue" :foreground "black")
  (set-face-attribute 'hl-line nil :background "RoyalBlue4")
  (set-face-attribute 'mode-line nil :background "black" :foreground "gray24")
  (set-face-attribute 'vertical-border nil :foreground "black")

  (defun gpc/set-font-size (size)
	"Set the default font fro the current frame to the given
SIZE."
	(interactive (list 
				  (let ((default
						  (round (face-attribute 'default :height) 10)))
					(read-number "Font size: " default))))
	;; some os's ignore size, so height is needed
	(set-face-attribute 'default (selected-frame) :height (* 10 size))
	(message "Font size set to %s" size))

  (defun gpc/increment-font-size()
	"Boost the default font size by 2."
	(interactive)
	(gpc/set-font-size (+ 2 (round (face-attribute 'default :height) 10))))

  (defun gpc/decrement-font-size()
	"Boost the default font size by -2."
	(interactive)
	(gpc/set-font-size (+ -2 (round (face-attribute 'default :height) 10))))


  (defun gui-adjust-to-screen-size ()
	"Use a larger font for a larger screen."
	(interactive)
	(let ((large-screen (>= 1680 (display-pixel-width)))
		  (larger-fonts t))
	  (let ((font-size (- (if large-screen 22 15)
						  (if (and larger-fonts
								   (not large-screen)) 4 0))))
		;; Note that `window-system' is deprecated in favor of `(display-graphic-p)'.
		;; https://news.ycombinator.com/item?id=29727195
		(if (or (eq window-system 'ns) (eq window-system 'w32))
			(gpc/set-font-size font-size)))))

  (gui-adjust-to-screen-size)

  ;; should add to my key map instead?  then you'd have to run it first
  (global-set-key (kbd "C-M-<") 'transparency-increase)
  (global-set-key (kbd "C-M->") 'transparency-decrease)
  ;; yeah, I have a hard time hitting this
  (global-set-key (kbd "<f11>") 'toggle-frame-fullscreen)
  (global-set-key (kbd "<f12>") 'toggle-frame-fullscreen)
  (global-set-key (kbd "C-M-S-SPC") 'toggle-fixed-pitch)
  (global-set-key (kbd "M-+") 'gpc/increment-font-size)
  (global-set-key (kbd "M-_") 'gpc/decrement-font-size)
  ;; intended only for Linux
  (set-face-attribute 'default nil :height (* 10 16))
  ;; “fullscreen” is a different beast on Mac OS.
  (unless (eq system-type 'darwin) (toggle-frame-fullscreen))
  )

(if (display-graphic-p)
	(gpc/gui-init)
  (if (facep 'hl-line) (set-face-background 'hl-line "blue")))

(prefer-coding-system 'utf-8-unix)
(setq require-final-newline nil)

(defun my-find-file-check-make-large-file-read-only-hook ()
  "If a file is over a given size, make the buffer read only."
  (when (> (buffer-size) (* 10 1024 1024))
	(setq buffer-read-only t)
	(buffer-disable-undo)
	(fundamental-mode)
	(message "Buffer is set to read-only because it is large.  Undo also disabled.")))

(add-hook 'find-file-hook 'my-find-file-check-make-large-file-read-only-hook)

(defun dot-emacs (&optional relative-path)
  "Return the full path of a file in the user's emacs directory."
  (expand-file-name (concat user-emacs-directory relative-path)))


(defun gpc/server-restart ()
  "Delete emacs server file and start server.  This is needed if
the last server instance didn't exit normally."
  (interactive)
  (shell-command (concat "rm " (dot-emacs "server/server")))
  (server-start))

(defalias 'ss 'gpc/server-restart)
(defalias 'trs 'tide-restart-server)
(defalias 'omr 'org-mode-restart)

(defalias 'dps 'docker-ps)


;; This doesn't quite work.  I need something like it when developing hooks, but
;; end up writing a mode-specific version of this.  Maybe a macro would do it.
;; (defun gpc/clear-hooks(mode)
;;   "Remove all hooks for mode MODE"
;;   (interactive)
;;   (while
;; (let* ((mode "nxml")
;;         (hook-name (concat mode "-mode-hook"))
;;         (hook (make-symbol hook-name)))
;;   (remove-hook hook (first (symbol-value hook))))

(defun gpc/shell-execute-buffer-file()
  "Saves current buffer file and executes it in default shell."
  (interactive)
  (save-buffer)
  (shell-command (shell-quote-argument (buffer-file-name))))

(defun gpc/powershell-execute-buffer-file()
  "Saves current buffer file and executes it in Windows powershell."
  (interactive)
  (save-buffer)
  (async-shell-command (concat "powershell -File " (buffer-file-name))))

(defun gpc/shell-explore-current-directory()
  "Opens the current directory in Windows explorer (assumes
  'start' command or equivalent)."
  (interactive)
  (save-buffer)
  (async-shell-command "start ."))

(defun gpc/vc-dir-root ()
  "Perform `vc-dir' at the project's root (currently hg or git)."
  (interactive)
  (vc-dir
   (replace-regexp-in-string
	"\n*$" ""
	(shell-command-to-string "hg root 2>/dev/null || git rev-parse --show-toplevel"))))

(defun gpc/vc-dir-root-git ()
  "Perform `vc-dir' at the project's root (for git)."
  (interactive)
  (let ((root-command "git rev-parse --show-toplevel"))
	(vc-dir
	 (replace-regexp-in-string
	  "\n*$" ""
	  (shell-command-to-string root-command)))))

(defun set-window-width (n)
  "Set the selected window's width."
  (interactive)
  (adjust-window-trailing-edge (selected-window) (- n (window-width)) t))
(defun rightsize-window ()
  "Set the selected window to the current fill column plus one."
  (interactive)
  (set-window-width (+ 1 fill-column)))

;; WHY is this needed? css has electric braces, right?
(defun gpc/css-insert-lcurly ()
  "Adds opening and closing braces with indent and cursor positioning in CSS mode."
  (interactive)
  (insert " {\n\n}")
  (indent-for-tab-command)
  (forward-line -1)
  (indent-for-tab-command))

(defun other-window-or-buffer ()
  "Cycle through open windows (if more than one), or switch to
  the most recently visited buffer."
  (interactive)
  ;; proxy for whether 1 window is displayed
  (if (equal (window-list) (list (next-window)))
	  (switch-to-buffer (other-buffer))
	(other-window 1)))

;; http://www.emacswiki.org/emacs/ElispCookbook#toc5
(defun chomp (str)
  "Chomp leading and tailing whitespace from STR."
  (let ((s (if (symbolp str) (symbol-name str) str)))
	(replace-regexp-in-string "\\(^[[:space:]\n]*\\|[[:space:]\n]*$\\)" "" s)))

(defun insert-timestamp ()
  "Inserts the current time and date in the format 2:10 PM 03/03/12"
  (interactive)
  (insert (chomp (format-time-string "%l:%M %p %D"))))

(defun insert-iso-date ()
  "Inserts the current date in the format 2020-12-18"
  (interactive)
  (insert (chomp (format-time-string "%F"))))


(defun insert-quotation-marks (&optional arg)
  "Inserts a pair of “curly” quotation marks around point or region.
  With optional ARG, use ‘single’ quotation marks."
  (interactive "P")
  (if arg
	  (insert-pair nil ?\u2018 ?\u2019)
	(insert-pair nil ?\u201C ?\u201D)))


(defun gpc/log-changeset (&optional repo)
  "Add the latest hg changeset id to the 'changesets' property of
  the current org task, using a comma-delimited list if a value is
  already present.  Prompts for the path to the repo location.  If
  not provided, defaults to the current directory."
  (interactive (list (read-directory-name "Repo path: " "../local")))
  (org-clock-goto)
  (let* ((prop "changesets")
		 (oldrevs (org-entry-get (point) prop))
		 (newrev (substring
				  (shell-command-to-string
				   (concat "hg id " (when repo (concat " -R " repo))
						   " | grep -o '^[^+ ]*'"))
				  0 -1)))
	(org-set-property prop
					  (concat oldrevs
							  (if oldrevs ",")
							  newrev))
	(message "Logged changeset %s" newrev)))

(defun gpc/visit-issue(&optional issue)
  "Browse to the URL for the given issue on an issue tracker.
   The URL will be constructed by formatting the given ISSUE
   identifier with the file's `issue-url-format' property.  If
   the file has no such property, the default is
   `https://bitbucket.org/issue/%s' If no ISSUE id is provided,
   the `issue' property from the current org task is used."
  (interactive (list 
				(let ((default
						(or
						 (org-entry-get (point) "issue" t)
						 (org-entry-get (point) "gitlab-issue" t))))
				  (read-string "Issue #: " default))))
  (let* ((url-format (or (org-entry-get (point)
										(if (org-entry-get (point) "gitlab-issue" t)
											"gitlab-issue-url-format"
										  "issue-url-format"
										  )
										t)
						 "https://bitbucket.org/issue/%s"))
		 (url (format url-format issue)))
	(browse-url url)))

(require 'thingatpt)
(defun gpc/etymology (word)
  "Browse to `etymonline.com' for a given word, defaulting to the
word under the cursor."
  (interactive (list 
				(let ((default (word-at-point)))
				  (read-string "Word: " default))))
	(browse-url (concat "http://etymonline.com?search=" word)))

(defun gpc/thesaurus (word)
  "Browse to `thesaurus.com' for a given word, defaulting to the
word under the cursor."
  (interactive (list 
				(let ((default (word-at-point)))
				  (read-string "Word: " default))))
	(browse-url (concat "https://onelook.com/thesaurus/?s=" word)))

(defun gpc/wiktionary (word)
  "Browse to `en.wiktionary.org' for a given word, defaulting to the
word under the cursor."
  (interactive (list 
				(let ((default (word-at-point)))
				  (read-string "Word: " default))))
	(browse-url (concat "https://en.wiktionary.org/wiki/" word)))

(defun gpc/rhymezone (word)
  "Browse to `rhymezone.com' for a given word, defaulting to the
word under the cursor."
  (interactive (list
				(let ((default (word-at-point)))
				  (read-string "Word: " default))))
	(browse-url (concat "http://rhymezone.com/r/rhyme.cgi?typeofrhyme=perfect&org1=syl&org2=l&org3=y&Word=" word)))


;; This would be better if it worked on numbers in the middle of tokens as well
(defun gpc/nap-inc (&optional arg)
  "If a number is at point, then increment it in-place by ARG."
  (interactive "p")
  (let ((nap (number-at-point)))
	(when nap
	  (let* ((bounds (bounds-of-thing-at-point 'sexp))
			 (from (car bounds))
			 (to (cdr bounds))
			 (old-point (point)))
		  (delete-region from to)
		  (goto-char from)
		  (insert (format "%s" (+ (or arg 1) nap)))
		  ;; Stay at the beginning instead of preserving old point.  Avoids the
		  ;; number “falling out of point” when the number of digits decreases.
		  ;; Would be more elegant to just nudge it back in.
		  ;; 
		  (goto-char from)
		  ;;(goto-char old-point)
		  ))))

(defun gpc/nap-dec (&optional arg)
  "If a number is at point, then decrement it in-place by ARG."
  (interactive "p")
  (gpc/nap-inc (- arg)))



;; http://stackoverflow.com/questions/1510091/with-emacs-how-do-you-swap-the-position-of-2-windows
(defun transpose-windows ()
  (interactive)
  (let ((this-buffer (window-buffer (selected-window)))
		(other-buffer (prog2
						  (other-window +1)
						  (window-buffer (selected-window))
						(other-window -1))))
	(switch-to-buffer other-buffer)
	(switch-to-buffer-other-window this-buffer)
	(other-window -1)))


(defun find-overlays-specifying (prop pos)
  (let ((overlays (overlays-at pos))
		found)
	(while overlays
	  (let ((overlay (car overlays)))
		(if (overlay-get overlay prop)
			(setq found (cons overlay found))))
	  (setq overlays (cdr overlays)))
	found))

(defun highlight-or-dehighlight-line ()
  (interactive)
  (if (find-overlays-specifying
	   'line-highlight-overlay-marker
	   (line-beginning-position))
	  (remove-overlays (line-beginning-position) (+ 1 (line-end-position)))
	(let ((overlay-highlight (make-overlay
							  (line-beginning-position)
							  (+ 1 (line-end-position)))))
	  (overlay-put overlay-highlight 'face '(:background "lightgreen"))
	  (overlay-put overlay-highlight 'line-highlight-overlay-marker t))))

(defun gpc/start-current-directory ()
  "Runs 'start .' in the default shell."
  (interactive)
  (shell-command "start ."))

(defun grep-view ()
  "Set view for browsing grep results."
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (rightsize-window)
  (other-window 1)
  (switch-to-buffer "*grep*")
  (goto-char (point-min)))

(defalias 'gv 'grep-view)

;; gives type error until rgrep has run separately at least once...
;; (setq debug-on-error t)
(defun simple-rgrep (pattern)
  "Run `rgrep' without prompting file path or pattern."
  (interactive "sSearch for: ")
  (rgrep pattern "*" ".")
  (grep-view))

(defalias 'gg 'simple-rgrep)

(defun code-view ()
  "Set view for viewing code with standard column width."
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (rightsize-window))

(defalias 'cv 'code-view)

(defun indent-xml ()
  "Non-semantically indents some xml for quick viewing.  Does not
  properly deal with angle brackets in content."
  (interactive)
  (fundamental-mode)
  (goto-char (point-min))
  (while (search-forward "><" nil t)
	(replace-match ">
  <" nil t))
  (nxml-mode)
  (call-interactively 'mark-whole-buffer)
  (indent-for-tab-command))

(defalias 'ix 'indent-xml)

(defun indent-json ()
  "Non-semantically indents some json for quick viewing.  Does not
  properly deal with quoted braces."
  (interactive)
  (fundamental-mode)
  (goto-char (point-min))
  (while (search-forward "{" nil t)
	(replace-match "{
  " nil t))
  (goto-char (point-min))
  (while (search-forward "}" nil t)
	(replace-match "
  }" nil t))
  (goto-char (point-min))
  (while (search-forward "\"," nil t)
	(replace-match "\",
  " nil t))
  (js2-mode)
  (call-interactively 'mark-whole-buffer)
  (indent-for-tab-command))

(defalias 'ij 'indent-json)


;; ARG doesn't transmit to function
(defun gpc/kill-line-or-region (&optional ARG)
  "Same as `kill-region' if a region is active; otherwise same as `kill-line'."
  (interactive)
  (if mark-active
	  (kill-region (region-beginning) (region-end))
	;; /// should call whatever was bound to C-k (in case mode overrode)
	(kill-line ARG)))

(declare-function vc-ensure-vc-buffer "vc")
(defun vc-diff-file-or-directory ()
  "Call `vc-diff' on buffer file, or `vc-dir' if not on a tracked
    file."
  (interactive)
  (unless
	  (condition-case ex
		  (vc-ensure-vc-buffer)
		('error
		 (vc-dir default-directory)
		 t))
	(vc-diff nil)))

(defun set-coding-system-to-dos ()
  "Sets utf-8-dos as the coding system for the currently visited file."
  (interactive)
  (set-buffer-file-coding-system 'utf-8-dos))

(defalias 'dd 'set-coding-system-to-dos)

;; http://stackoverflow.com/a/4459159/4525
(defun toggle-fold ()
  "Toggle fold all lines larger than indentation on current line"
  (interactive)
  (let ((col 1))
	(save-excursion
	  (back-to-indentation)
	  (setq col (+ 1 (current-column)))
	  (set-selective-display
	   (if selective-display nil (or col 1))))))

(declare-function toggle-fixed-pitch "gui-init")
(defun gpc/test-fill ()
  "Quick check for whether you're passing `fill-column'."
  (interactive)
  (move-beginning-of-line nil)
  (open-line 1)
  (insert (make-string fill-column ?*))
  (toggle-fixed-pitch))

(defun gpc/stylus-print-css()
  "Save and compile current stylus file to output"
  (interactive)
  (save-buffer)
  (shell-command (concat "stylus " buffer-file-name " --print")))

(defun gpc/uglify-js()
  "Save and compile current javascript file to output"
  (interactive)
  (save-buffer)
  (shell-command (concat "uglifyjs " buffer-file-name " --compress --mangle")))

(defvar xslt-input "~/test.xml"
  "The default input to use in `gpc/run-xslt'.  Buffer-local in
    '.xsl' files (in `nxml-mode').  Set interactively using
    `gpc/set-xslt-input'.")

(defvar xslt-autorun nil
  "Whether to run XSLT files on save (when `xslt-input' is set).")


(defun gpc/set-xslt-input(file)
  "Sets a file location to use as the current buffer's input file
    with `gpc/run-xslt'."
  (interactive (list 
				(let ((default
						"~/test.xml"))
				  (read-file-name "XML file: "
								  (file-name-directory xslt-input) xslt-input))))
  (setq xslt-input file)
  (gpc/run-xslt))

(defun gpc/run-xslt()
  "Execute XSLT in buffer against a designated input, using xslt
    shell command."
  (interactive)
  (when xslt-input
	(ignore-errors
	  (async-shell-command
	   (concat "cat " xslt-input
			   " | xsltproc \"" (buffer-file-name) "\" -")
	   "*XSLT*")))
  t)

;; http://stackoverflow.com/a/14769115/4525
(defun local-set-minor-mode-key (mode key def)
  "Overrides a minor mode keybinding for the local
     buffer, by creating or altering keymaps stored in buffer-local
     `minor-mode-overriding-map-alist'."
  (let* ((oldmap (cdr (assoc mode minor-mode-map-alist)))
		 (newmap (or (cdr (assoc mode minor-mode-overriding-map-alist))
					 (let ((map (make-sparse-keymap)))
					   (set-keymap-parent map oldmap)
					   (push `(,mode . ,map) minor-mode-overriding-map-alist) 
					   map))))
	(define-key newmap key def)))



(defun gpc/dot-view ()
  "Compile and preview the current `dot' buffer."
  (interactive)
  (compile compile-command)
  (graphviz-dot-preview))

;; (add-hook 'after-save-hook 'gpc/dot-view t t)

;; Mode hooks and supporting functions (when it's really clearer to have them
;; here).

(defvar nxml-child-indent)
(defvar global-hl-line-mode)

;; assigned
(defvar rng-nxml-auto-validate-flag)
(defvar js2-global-externs)

;; TODO: doesn't leave region marked.  can't figure out how to do that.
(defun move-region-up (arg)
  "Move the current region up one line."
  (interactive "p")
  (when (use-region-p)
	(let ((text (delete-and-extract-region (point) (mark))))
	  ;; (if (zerop (current-column)) 0 1)
	  (beginning-of-line 1)
	  (let ((pos (point)))
		;;(beginning-of-line (- 1 arg))
		(activate-mark)
		(insert text)
		(set-mark (+ pos (length text)))
		(set-window-point nil pos)
		(exchange-point-and-mark)
		))))

(defun move-region-down (arg)
  "Move the current region down one line."
  (interactive "p")
  (message "move-region-down not implemented"))

(defun move-line-or-region-up (arg)
  "Move the current line or region up one line."
  (interactive "p")
  (if (use-region-p)
	  (move-region-up arg)
	(gpc/move-line-up arg)))

(defun move-line-or-region-down (arg)
  "Move the current line or region down one line."
  (interactive "p")
  (if (use-region-p)
	  (move-region-down arg)
	(gpc/move-line-down arg)))

;; Taken from "old" version of org-mode (the one that ships with Emacs 23).
(defun gpc/move-line-down (arg)
  "Move the current line down.  With prefix argument, move it past ARG lines."
  (interactive "p")
  (let ((col (current-column))
		beg end pos)
	(beginning-of-line 1) (setq beg (point))
	(beginning-of-line 2) (setq end (point))
	(beginning-of-line (+ 1 arg))
	(setq pos (move-marker (make-marker) (point)))
	(insert (delete-and-extract-region beg end))
	(goto-char pos)
	(org-move-to-column col)))

(defun gpc/move-line-up (arg)
  "Move the current line up.  With prefix argument, move it past ARG lines."
  (interactive "p")
  (let ((col (current-column))
		beg end pos)
	(beginning-of-line 1) (setq beg (point))
	(beginning-of-line 2) (setq end (point))
	(beginning-of-line (- arg))
	(setq pos (move-marker (make-marker) (point)))
	(insert (delete-and-extract-region beg end))
	(goto-char pos)
	(org-move-to-column col)))

(declare-function vc-dir-marked-files "vc-dir")
(defun gpc/vc-dir-delete-unregistered-file ()
  "For unregistered files only!  Will plain-delete VC-ed files
  with no backup.  Delete the marked files, or the current file if
  no marks."
  (interactive)
  (let ((files (or (vc-dir-marked-files)
				   (list (vc-dir-current-file)))))
	(when (yes-or-no-p (format "Delete %d files?" (length files)))
	  (mapc 'delete-file files)
	  (revert-buffer))))

;; This isn't working, like anywhere
(defun gpc/bindkey-comment-region ()
  "Bind C-c C-c to `comment-region'."
  (local-set-key (kbd "C-c C-c") 'comment-region))

;; TODO: make a version that moves whole region
(defun gpc/bindkeys-move-line ()
  (local-set-key (kbd "M-p") 'gpc/move-line-up)
  (local-set-key (kbd "M-n") 'gpc/move-line-down))

(add-hook 'python-mode-hook
		  'gpc/bindkey-comment-region)

(add-hook 'text-mode-hook
		  'gpc/bindkeys-move-line)

(add-hook 'c-mode-common-hook
		  '(lambda ()
			 (setq indent-tabs-mode t)
			 (gpc/bindkey-comment-region)
			 ))

;;(add-hook 'compilation-mode-hook 'next-error-follow-minor-mode)

;; http://gnu.emacs.bug.narkive.com/WR7kCPrW/bug-18444-24-3-93-error-running-timer-compilation-auto-jump-from-grep-mode
;; But it doesn't work... but not needed now, right?  this was the debug on error thing, I think.
;;
;; Something has been screwy with grep mode following, and I suspect this is related.
;; (add-hook 'grep-mode-hook
;;           (lambda()
;;             (kill-local-variable 'compilation-auto-jump-to-next))
;;           )

(defvar whitespace-style)
(defvar whitespace-display-mappings)
(eval-after-load "whitespace"
  '(progn
	 (setq whitespace-style '(face indentation tabs tab-mark))
	 (setq
	  whitespace-display-mappings
	  ;; all numbers are Unicode codepoint in decimal
	  '(
		(space-mark 32 [183] [46]) ; 183 MIDDLE DOT, 46 FULL STOP
		(newline-mark 10 [182 10]) ; 182 PILCROW SIGN
		;; (tab-mark 9 [9655 9] [92 9]) ; 9655 WHITE RIGHT-POINTING TRIANGLE
		;;(tab-mark 9 [9615 9] [92 9]) ; 9655 WHITE RIGHT-POINTING TRIANGLE
		(tab-mark 9 [9612 9] [92 9]) ; 9655 WHITE RIGHT-POINTING TRIANGLE
		;;(tab-mark 9 [9608 9] [92 9]) ; 9655 WHITE RIGHT-POINTING TRIANGLE
		))))


(add-hook 'lisp-mode-hook
		  '(lambda ()
			 (gpc/bindkey-comment-region)
			 ))

;; per https://github.com/jscheid/prettier.el#configuration
;; 
;; Note that calling `prettier-mode' specifically does not seem (in my
;; experience) to pick up the configured options as expected.  This does,
;; though, and recognizes all the modes that I use.
(add-hook 'after-init-hook #'global-prettier-mode)

;; https://github.com/ananthakumaran/tide
(defun setup-tide-mode ()
  (interactive)
  ;; Moved this here because it seemed to be resetting
  (setq tide-tsserver-locator-function #'gpc/tide-locate-tsserver)
  ;; Same here, re resetting
  ;; Otherwise `tide-project-errors' reports lots of things that it shouldn't.
  ;; See https://github.com/ananthakumaran/tide/issues/312
  (setq tide-disable-suggestions t)
  (tide-setup)
  (local-set-key (kbd "<C-return>") 'tide-fix)
  (local-set-key (kbd "M-g d") 'tide-documentation-at-point)
  (local-set-key (kbd "M-g e") 'tide-project-errors)
  (local-set-key (kbd "M-g r") 'tide-references)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (company-mode +1)
  )


;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

(require 'web-mode)
;; we get formatting from prettier, and this gets really slow
(setq web-mode-enable-auto-indentation nil)
(add-hook 'web-mode-hook
          (lambda ()
            (when (string-equal "tsx" (file-name-extension buffer-file-name))
              (setup-tide-mode))))

;;(require 'yaml-mode)
;;(eval-after-load "yaml"
;;  '(progn
;;	 (define-key yaml-mode-map [remap fill-paragraph] 'yaml-fill-paragraph)))

(add-hook 'typescript-mode-hook #'setup-tide-mode)

(add-hook 'js2-mode-hook
		  '(lambda ()
			 (gpc/bindkey-comment-region)
			 (local-set-key (kbd "M-g j") 'js2-jump-to-definition)
			 ))

(add-hook 'csharp-mode-hook
		  '(lambda ()
			 (local-set-key (kbd "C-c c") 'gpc/linqpad)
			 (local-set-key (kbd "{") 'self-insert-command)
			 ))

(add-hook 'sh-mode-hook
		  '(lambda ()
			 (gpc/bindkey-comment-region)
			 (setq tab-width 2)
			 (setq indent-tabs-mode nil)
			 (local-set-key (kbd "C-c c") 'gpc/shell-execute-buffer-file)
			 (prettier-mode 0)			; global prettier is enabling this for some reason
			 ))

(add-hook 'powershell-mode-hook
		  '(lambda ()
			 (gpc/bindkey-comment-region)
			 (local-set-key (kbd "C-c c") 'gpc/powershell-execute-buffer-file)
			 ))

(defun gpc/refresh-inline-images ()
  (when org-inline-image-overlays
	(org-redisplay-inline-images)))

(eval-after-load "org"
  '(progn
	 (add-hook 'org-babel-after-execute-hook 'gpc/refresh-inline-images)
	 (require 'ox-md nil t)
	 ))

(eval-after-load "js2-mode"
  '(progn
	 (setq js2-strict-trailing-comma-warning nil)
	 ))

(add-hook 'org-mode-hook
		  (lambda ()
			(org-indent-mode t)
			;; This makes org-mode super slow and crasy on my new machine for
			;; some reason.  See https://github.com/sabof/org-bullets/issues/11
                        (unless (equal (upcase system-name) "BENEDICK")
                          (if (package-installed-p 'org-bullets)
                              (org-bullets-mode t)))
			(visual-line-mode t)
			(auto-fill-mode t)
			(local-set-key (kbd "M-p") 'org-metaup)     ; outline shortcuts
			(local-set-key (kbd "M-n") 'org-metadown)
			(local-set-key (kbd "M-P") 'org-promote-subtree)
			(local-set-key (kbd "M-N") 'org-demote-subtree)
			(local-set-key (kbd "C-c l") 'org-insert-link-from-clipboard)
			(local-set-key (kbd "C-c i") 'gpc/visit-issue)
			(local-set-key (kbd "C-c f") 'org-footnote-new)
			))

(add-hook 'org-agenda-mode-hook
		  (lambda ()
			(local-set-key (kbd "=") 'org-agenda-priority-up)
			))

(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

(add-hook 'diff-mode-hook
		  (lambda ()
			(local-set-key (kbd "j") 'diff-goto-source)
			))


(defun gpc/dired-drill ()
  "Expand dired directories into successive panels."
  ;; Could toggle by direction, or base default on metrics of window
  (interactive)
  (dired-hide-details-mode t)
  (split-window-horizontally)
  (other-window-or-buffer)
  (dired-find-file)
  (balance-windows))

(defun gpc/dired-shell-execute-file()
  "Open the marked file in the system shell (i.e. using the
associated application)."
  (interactive)
  ;; Doesn't work as expected in Linux with `dired-do-async-shell-command'. See:
  ;; 
  ;; https://groups.google.com/d/msg/gnu.emacs.help/7XYobqGxbbg/BsrnJgpb6NYJ
  ;; https://bugzilla.gnome.org/show_bug.cgi?id=652262
  ;; http://lists.gnu.org/archive/html/emacs-devel/2009-07/msg00279.html
  (dired-do-shell-command
   (cond
   	((eq system-type 'windows-nt) "start")
   	((eq system-type 'darwin) "open")
   	(t "xdg-open")
   	)
   nil
   (dired-get-marked-files)))

(add-hook 'dired-mode-hook
		  (lambda ()
			(diff-hl-dired-mode t)
			(local-set-key (kbd "e") 'dired-up-directory)
			(local-set-key (kbd ";") 'isearch-forward)
			(local-set-key (kbd "C-c C-c") 'wdired-change-to-wdired-mode)
			(local-set-key (kbd "W") 'gpc/dired-shell-execute-file)
			(local-set-key (kbd "r") 'gpc/dired-drill)
			))

(add-hook 'wdired-mode-hook
		  (lambda ()
			(local-set-key (kbd "C-m") 'wdired-finish-edit)
			))

(add-hook 'vc-dir-mode-hook
		  (lambda ()
			;; TODO: also enable delete directly from vc-dir
			(local-set-key "r" 'vc-revert)
			(local-set-key "d" 'gpc/vc-dir-delete-unregistered-file)
			))

(add-hook 'nxml-mode-hook
		  (lambda ()
			(gpc/bindkey-comment-region)
			(gpc/bindkeys-move-line)
			(setq indent-tabs-mode t)
			(setq rng-nxml-auto-validate-flag nil)
			(setq tab-width nxml-child-indent)
			;; For some reason this works better than `nxml-complete'
			(local-set-key (kbd "C-M-/") 'rng-complete)
			(local-set-key (kbd "M-?") 'rng-complete)
			(when buffer-file-name
			  (let ((is-xslt-file (string-match "\\.xsl$" (buffer-file-name))))
				(when is-xslt-file
				  (make-local-variable 'xslt-input)
				  ;;(setq-default xslt-input "~/test.xml")
				  
				  (make-local-variable 'xslt-autorun)
				  ;;(setq-default xslt-autorun nil)
				  
				  (local-set-key (kbd "C-c c") 'gpc/run-xslt)
				  (add-hook 'after-save-hook
							(lambda ()
							  (when
								  (and (string-match "\\.xsl$" (buffer-file-name))
									   xslt-autorun)
								(ignore-errors (gpc/run-xslt))))))))
			))

(add-hook 'xbase-mode-hook
		  (lambda ()
			(setq indent-tabs-mode t
				  tab-width 4
				  ;; xbase-indent-line doesn't respect indent-tabs-mode
				  indent-line-function 'indent-relative
				  comment-start "*")
			(gpc/bindkey-comment-region)
			(gpc/bindkeys-move-line)
			(local-set-key (kbd "TAB") 'self-insert-command)
			))

(defun gpc/sws-dendent()
  "Stub buggy function to prevent crash in sws-mode."
  (interactive)
  (when (region-active-p)
	(let* ((text (buffer-substring (mark) (point)))
		   (start (min (mark) (point)))
		   (end (max (mark) (point)))
		   (undented (replace-regexp-in-string "^\t" "" text))
		   )
	  (kill-region (mark) (point))
	  (insert undented)
	  (set-window-point nil start)
	  ;; yeah, I know
	  (set-mark (- end (- (length text) (length undented))))
	  )))


(defun start-css-extensions()
  "Start minor modes that enhance CSS editing."
  (gpc/bindkey-comment-region)
  (gpc/bindkeys-move-line)
  (setq indent-tabs-mode nil)
  ;; sws-mode also turns this off...
  ;; (setq tab-width 4)
  ;; (whitespace-mode t)
  )

(add-hook 'stylus-mode-hook
		  '(lambda ()
			 (local-set-key (kbd "C-c c") 'gpc/stylus-print-css)
			 ; (local-set-key [backtab] 'gpc/sws-dendent)
			 (setq sws-tab-width 2)
			 (setq tab-always-indent nil) ; stopgap
			 (start-css-extensions)
			 ;; For now, makes tab not work right
			 ;;(yas-minor-mode -1)
			 ))

;; C-. and C-> don't work in terminal on linux.  Neither do some default
;; keybindings like M-S-u and M-S-d.
;;
;; reserve C-[0-9] for their default binding (digit-argument)
(require 'cl)

(setq org-agenda-archives-mode t)

(defvar org-agenda-category-filter)

(defvar gpc/last-todo-cat)

(defun gpc/buffers ()
  "View and activate file buffer list."
  (interactive)
  ;; this is crashing when given an argument since using x64 build.  You just
  ;; have to press T after the buffer menu loads.
  (list-buffers)
  (other-window-or-buffer))

(defvar my-keys-minor-mode-map (make-sparse-keymap) "My keymap.")
(defvar gpc/quick-key-map (make-sparse-keymap) "Two-step keymap")

(defun gpc/dired-here ()
  "Run `dired' in the current directory."
  (interactive)
  (dired "."))

;; ** Global key bindings **

;; don't know how to do this in the list
(define-key my-keys-minor-mode-map (kbd "M-<f1>") help-map)

(loop for (key . def) in
	  '(
		("C-h" . backward-delete-char)
		("C-M-i" . toggle-fold)
		("M-C-y" . kill-ring-search)
		("C-k" . gpc/kill-line-or-region)
		("C-S-l" . recenter-top-bottom)
		("C-l" . smex)                                  ; execute-extended-command
		("C-c w" . gpc/wiktionary)
		("C-c e" . gpc/etymology)
		("C-c r" . gpc/rhymezone)
		("C-c t" . gpc/thesaurus)
		("C-w" . backward-kill-word)
		("M-s" . isearch-forward-regexp)
		("M-r" . isearch-backward-regexp)
		("M-\"" . insert-quotation-marks)
		;;("M-`" . other-frame)
		;; For mac
		("M-`" . switch-to-buffer)
		("C-`" . switch-to-buffer)
		("C-x ?" . what-cursor-position)
		;; This isn't working on the mac with Caps as ctrl (but works with fn as ctrl)
		;; oh... mac takes it over
		;; ("C-M-d" . down-list)
		;; https://apple.stackexchange.com/a/114269
		;; cant believe I've been living without this for so long
		;; defaults write com.apple.symbolichotkeys AppleSymbolicHotKeys -dict-add 70 '<dict><key>enabled</key><false/></dict>'

		("C-M-=" . count-lines-region)
		;; ("M-=" . vc-diff-file-or-directory)
		("M-=" . magit-diff-buffer-file)
		("M-1" . delete-other-windows)
		("M-2" . split-window-vertically)
		("M-3" . split-window-horizontally)
		("M-4" . find-file)
		("M-5" . follow-delete-other-windows-and-split)
		("M-6" . save-buffer)
		("M-o" . other-window-or-buffer)
		("C-c a" . org-agenda)
		("C-c C-x C-j" . org-clock-goto)
		("C-c C-x C-k" . org-store-link)
		("C-x v x" . ediff-revision)
		("<C-wheel-down>" . text-scale-decrease)
		("<C-wheel-up>" . text-scale-increase)
		("M-7" . winner-redo)
		("M-8" . winner-undo)
		("M-9" . previous-buffer)
		("M-0" . next-buffer)
		("C-|" . mc/edit-lines)
		("C-S-SPC" . mc/mark-all-dwim)
		("S-<f1>" . (lambda() (interactive)
					  (org-agenda-list)
					  (delete-other-windows)))
		("<f2>" . bookmark-jump)
		("<f5>" . profiler-report)
		;; Having trouble with F4 key on the mac somehow
		("<f5>" . kmacro-end-or-call-macro)
		("<f8>" . save-buffer)
		("<f9>" . goto-last-change)
		("<f10>" . save-buffer)

		("M-]" . forward-paragraph)
		("M-[" . backward-paragraph)
		("M-P" . gpc/nap-inc)
		("M-N" . gpc/nap-dec)
		("C-M-S-p" . (lambda (&optional n) (interactive "p") (scroll-down n)))
		("C-M-S-n" . (lambda (&optional n) (interactive "p") (scroll-up n)))
		("C-M-S-t" . transpose-windows)
		("C-x C-c" . kill-this-buffer)
		("<M-return>" . company-complete)
		)
	  do (define-key my-keys-minor-mode-map (read-kbd-macro key) def))

(define-minor-mode my-keys-minor-mode
  "A minor mode for my key settings, to override major modes."
  t "" my-keys-minor-mode-map)

(define-key gpc/quick-key-map (kbd "w") 'gpc/shell-execute-buffer-file)
(define-key gpc/quick-key-map (kbd "e") 'gpc/dired-here)
(define-key gpc/quick-key-map (kbd "r") 'string-rectangle)
(define-key gpc/quick-key-map (kbd "p") 'diff-hl-previous-hunk)
(define-key gpc/quick-key-map (kbd "n") 'diff-hl-next-hunk)

(define-key gpc/quick-key-map (kbd "a") 'org-agenda)
(define-key gpc/quick-key-map (kbd "f") 'org-capture)

(define-key gpc/quick-key-map (kbd "h") help-map)

(define-key gpc/quick-key-map (kbd "k") 'kill-this-buffer)
(define-key gpc/quick-key-map (kbd "l") 'smex) ;'execute-extended-command

(define-key gpc/quick-key-map (kbd "b") 'gpc/buffers)

(define-key gpc/quick-key-map (kbd "SPC") 'bookmark-jump)

(require 'key-chord)
(key-chord-mode 1)
(setq key-chord-two-keys-delay .05)
(key-chord-define my-keys-minor-mode-map "fj" gpc/quick-key-map)

;;(defvar gpc/alt-key-map (copy-keymap gpc/quick-key-map) "Alternate keymap")
;;(define-key gpc/alt-key-map (kbd "s") 'isearch-forward)
;;(key-chord-define my-keys-minor-mode-map "dk" gpc/alt-key-map)

(key-chord-define my-keys-minor-mode-map "j1" 'delete-other-windows)
(key-chord-define my-keys-minor-mode-map "j2" 'split-window-vertically)
(key-chord-define my-keys-minor-mode-map "j3" 'split-window-horizontally)

(key-chord-define my-keys-minor-mode-map "jr" 'rgrep)

(key-chord-define my-keys-minor-mode-map "fv" 'gpc/vc-dir-root)
;; (key-chord-define my-keys-minor-mode-map "fg" 'gpc/vc-dir-root-git)
(key-chord-define my-keys-minor-mode-map "fg" 'magit)

(my-keys-minor-mode 1)

;; THIS IS A COPY of the ergo-movement.el that is available in various
;; places.

(defvar ergo-movement-mode-map
  (let ((map (make-sparse-keymap))
		;; "keydefs" is an alist of (KEY . ACTION) elements. KEY is a
		;; keybinding string. ACTION can be a keybinding string or a
		;; symbol referring to a command. Keybinding strings use the
		;; format known by the "kbd" macro.
		(keydefs '(
				   ;; indent-new-comment-line -> backward-char
				   ("M-j" . "C-b")
				   ("M-J" . indent-new-comment-line)
				   ;; downcase-word -> forward-char
				   ("M-l" . "C-f")
				   ("M-L" . downcase-word)
				   ;; tab-to-tab-stop -> previous-line
				   ("M-i" . "C-p")
				   ("M-I" . tab-to-tab-stop)
				   ;; kill-sentence -> next-line
				   ("M-k" . "C-n")
				   ("M-K" . kill-sentence)
				   ;; indent-new-comment-line -> backward-word
				   ("C-M-j" . "M-b")
				   ;; reposition-window -> forward-word
				   ("C-M-l" . "M-f")
				   ("C-M-S-l" . reposition-window)
				   ))
		key def)

	(dolist (keydef keydefs)
	  (setq key (eval `(kbd ,(car keydef)))
			def (cdr keydef))
	  (cond ((commandp def t)
			 (define-key map key def))
			((stringp def)
			 (define-key map key
			   `(lambda () (interactive)
				  (call-interactively
				   (key-binding ,(read-kbd-macro def))))))))
	map))


  ;;;###autoload
(define-minor-mode ergo-movement-mode
  "Ergonomic keybindings for cursor movement

  Ergo Movement mode is a global minor mode which defines ergonomic
  keybindings for cursor movement. This is suitable for QWERTY
  keyboard.

               i      =          C-p
      M-     j k l    =    C-b   C-n   C-f

      C-M-   j   l    =    M-b         M-f

  The original bindings of the above movement commands are kept
  untouched. The new bindings override other commands though. Those
  commands are resurrected through shifted versions of their
  original keybindings.

  \\{ergo-movement-mode-map}"

  :global t
  :lighter ""
  :keymap ergo-movement-mode-map
  )


(provide 'ergo-movement-mode)

(require 'ergo-movement-mode)
(ergo-movement-mode t)

(if (not (package-installed-p 'yasnippet))
    (message "gpc: warning yasnippet is not installed, skipping configuration")
  (setq yas-snippet-dirs '("~/upstart/yasnippets"))
  (yas-global-mode))

;; ** Windows stuff **

(if (eq system-type 'windows-nt)

    (setq shell-file-name "bash"
          shell-command-switch "-c"
          explicit-shell-file-name shell-file-name)

  ;; Assumes EmacsW32 distribution
  (setq ange-ftp-ftp-program-name "C:/Program Files (x86)/Emacs/EmacsW32/gnuwin32/bin/ftp.exe")
  )

(require 'org)
(require 'org-clock)
(require 'ox)

;; Persist clock across sessions.
(setq org-clock-persist t)
(org-clock-persistence-insinuate)
(setq org-clock-into-drawer nil)		; default changed ~Org 8.3

;; c/o http://stackoverflow.com/q/22720526/4525
;; TEMP?  For LOE, but not good for my timesheets
;; Good riddance:
;; >  This variable is obsolete since Org 9.1;
;; >  set `org-duration-format' instead.
(setq org-time-clocksum-format
 	  (quote (:days "%gd" :require-days t)))
(setq org-time-clocksum-format
 	  (quote (:days "%gd" :require-days nil)))
(setq org-time-clocksum-format
 	  (quote (:days "%gd" :hours "%d" :minutes ":%02d")))
(setq org-time-clocksum-format nil)
(setq org-duration-format '((special . h:mm)))
(setq org-time-clocksum-use-fractional nil)
(setq org-time-clocksum-fractional-format (quote (:days "%gd" :require-days t)))
(setq org-time-clocksum-use-effort-durations nil)
(setq org-table-duration-custom-format 'hours)

;; “In Org 9.2 the format was changed”
(add-to-list 'org-structure-template-alist '("as" . "aside")) ; I want this to emit <aside>
(add-to-list 'org-structure-template-alist '("t" . "tbd")) ; this should emit <aside> with class tbd
(add-to-list 'org-structure-template-alist '("n" . "note"))
(add-to-list 'org-structure-template-alist '("d" . "dialog"))
(add-to-list 'org-structure-template-alist '("w" . "wide"))

;; That is the new format, but template expansion still doesn't work without
;; this.  c/o
;; https://github.com/syl20bnr/spacemacs/issues/11798#issuecomment-454941024
(when (version<= "9.2" (org-version))
  (require 'org-tempo))


(setq org-todo-keyword-faces
	  '(
		("TODO" . (:foreground "yellow" :weight bold))
		("NOW" . (:foreground "red1" :weight bold))
		("IDEA" . (:foreground "gold1" :weight bold))
		("PROPOSAL" . (:foreground "gold1" :weight bold))
		("DONE" . (:foreground "darkgreen" :weight bold))
		("BUG" . (:foreground "red3" :weight bold))
		("BUG-LO" . (:foreground "LightPink3" :weight bold))
		("MIGRATED" . (:foreground "gold1" :weight bold))
		("FIXED" . (:foreground "darkgreen" :weight bold))
		("WONTFIX" . (:foreground "tan1" :weight bold))
		("FAILED" . (:foreground "FireBrick" :weight bold))
		("ON-HOLD" . (:foreground "DarkGray" :weight bold))
		("NOT-TODO" . (:foreground "DarkSlateGray" :weight bold))
		("NOT-REPRO" . (:foreground "orange" :weight bold))
		))

(setq org-todo-keywords
	  '((sequence "TODO(t!)"
				  "IDEA(i!)"
				  "PROPOSAL(i!)"
				  "BUG(b!)"
				  "|"
				  "NOT-TODO(o!)"
				  "NOT-REPRO(r!)"
				  "DONE(d!)")))

(defun org-insert-link-from-clipboard (start end)
  "Make a link out of the region text using the target from the clipboard."
  (interactive "r")
  (if (region-active-p)
	  (let ((link-text (buffer-substring start end)))
		(delete-region start end)
		(insert
		 (org-make-link-string
		  (with-temp-buffer (clipboard-yank) (buffer-string))
		  link-text)))))

;; Literate programming settings
(setq org-src-preserve-indentation 0)
(setq org-src-fontify-natively t)
(setq org-pretty-entities t)
(setq org-hide-emphasis-markers t)
(setq org-image-actual-width nil)
(setq org-cycle-global-at-bob t)
(add-to-list 'org-src-lang-modes '("dot" . graphviz-dot))
(add-to-list 'org-src-lang-modes '("jsx" . rjsx))


;; You should name footnotes when using included documents, to avoid
;; collisions.
(setq org-footnote-auto-label 'confirm)
(setq org-footnote-auto-adjust t)

(setq org-use-speed-commands t)
;; org-speed-commands-user “is obsolete since 9.5”
;; (setq org-speed-commands '(("z" . org-add-note)))
;; (setq org-speed-commands '((" " . org-add-note)))
(setq org-speed-commands 
      (append
	   '(("my speed keys")
		 ;; Space is no longer working as a speed command, at least in Windows
         (" " . org-add-note)
         ("z" . org-add-note))
	   org-speed-commands))

;; Settings related to publication from `org-mode'.  Keeping this separate from
;; other configuration (including custom) .

(require 'org)

(defvar org-export-with-toc)
(defvar org-export-with-section-numbers)
(defvar org-export-with-smart-quotes)
(defvar org-html-postamble)
(defvar org-html-head-include-default-style)
(defvar org-html-head-include-scripts)
(defvar org-html-doctype)
(defvar org-html-html5-fancy)
(defvar org-html-container-element)
(defvar org-publish-project-alist)

;; THIS MAY also be a bug.
(defun gpc-ob-js-block-hack (args)
  (list (replace-regexp-in-string "\n" " " (first args))))

(advice-add 'org-babel-js-read :filter-args #'gpc-ob-js-block-hack)


;; TODO: submit bug.  There is no "checkmark" entity defined in any HTML DTD,
;; and it is not recognized by Chrome or Firefox (which render &check; and
;; &checkmark; as literal text).  Oddly, although it is this way in the current
;; source, the correct value is shown at
;; http://orgmode.org/worg/org-symbols.html
(push
 (mapcar
  (lambda (ele)
	(if (string= ele "&checkmark;") "&#10003;" ele))
  (assoc "check" org-entities))
 org-entities-user)

(add-to-list 'org-entities-user '("xmark" "\\xmark" t "&#x2717;" "X" "X" "✗"))

;; Unfortunately, this causes “Process 'org-export-process' exited abnormally”
;; (setq org-export-in-background t)

;; Global export settings.  These will affect exports not done through publish,
;; which is mostly what we want.  Where it's not, I don't know a good way to
;; share these between components without having to include, e.g. a SETUPFILE
;; directive in every document, which I don't want to require.
(setq org-export-with-toc nil)
(setq org-export-with-section-numbers nil)
(setq org-export-with-smart-quotes t)
(setq org-html-postamble nil)
(setq org-html-head-include-default-style nil)
(setq org-html-head-include-scripts nil)
(setq org-html-doctype "xhtml5")          ; just for well-formedness, but adds xmlns
(setq org-html-html5-fancy t)             ; only matters if you use e.g. BEGIN_ASIDE
(setq org-html-container-element "section")     ; only applies to top-level headlines
(setq org-html-htmlize-output-type 'inline-css) ;TEMP
(setq org-html-htmlize-output-type 'css)
(setq org-ditaa-jar-path (dot-emacs "ditaa.jar"))

(setq org-babel-use-quick-and-dirty-noweb-expansion t)

(require 'ox-latex)
;;(setq org-latex-listings t)
(setq org-latex-listings 'minted)
;; (add-to-list 'org-latex-packages-alist '("" "listings"))
;; (add-to-list 'org-latex-packages-alist '("" "color"))
(add-to-list 'org-latex-packages-alist '("" "svg"))
(add-to-list 'org-latex-packages-alist '("" "minted"))

(add-to-list 'org-latex-minted-langs '(xsl "xslt"))
(add-to-list 'org-latex-minted-langs '(tup "make")) ; closest in spirit
(add-to-list 'org-latex-minted-langs '(stylus "sass"))

(require 'ob-sql)
(require 'ob-js)

;; (require 'ob-sh)
;; Above crashes in latest version of org.
;; https://lists.gnu.org/archive/html/emacs-orgmode/2014-01/msg01008.html
;; https://lists.gnu.org/archive/html/emacs-orgmode/2017-07/msg00289.html
;; See also load-languages list below.  I hope this doesn't mean I
;; have to also change `sh` to `shell` for all language blocks.
;; See above, this isn't available
;;(require 'ob-shell)

(require 'ob-ditaa)
(require 'ob-dot)
(setq org-confirm-babel-evaluate nil)
(setq org-babel-check-confirm-evaluate nil)
(org-babel-do-load-languages
 'org-babel-load-languages '((ditaa . t) (dot . t) (js . t) (sql . t)))
;; because of above
;; 'org-babel-load-languages '((ditaa . t) (dot . t) (shell . t) (js . t) (sql . t)))

;; Emacs should never mess with directories where I'm editing
(setq create-lockfiles nil)

(setq gpc/emacs-backup-place "~/.emacs-backups")
(setq backup-directory-alist `(("." . ,gpc/emacs-backup-place)))
;; TODO: this is *still* not working reliably.  I'm still seeing auto-save files
;; for edited buffers in the same directory.
;;(setq auto-save-file-name-transforms `(("(.*)" "~/.emacs-backups/\\1" t)))
(setq auto-save-file-name-transforms `((".*" "~/.emacs-backups/" t)))

;; Was doing this before returning to monolithic init
;; (let ((default-directory (dot-emacs)))
;;   (normal-top-level-add-to-load-path '("lisp"))
;;   (normal-top-level-add-subdirs-to-load-path))

(normal-top-level-add-to-load-path (list (dot-emacs "packages")))


;; Was for system-specific stuff
;;(load (symbol-name system-type) t)

;; The custom setting is not working
(if (not (package-installed-p 'smart-mode-line))
    (message "gpc: warning: smart-mode-line is not installed, skipping configuration")
  (setq sml/no-confirm-load-theme t)
  (smart-mode-line-enable))

(require 'goto-last-change)

(if (not (package-installed-p 'auto-save-buffers-enhanced))
    (message "gpc: warning: auto-save-buffers-enhanced is not installed, skipping configuration")
  (require 'auto-save-buffers-enhanced)
  (setq auto-save-buffers-enhanced-include-regexps
	'(".*\.xml$"
	  ".*\.xsl$"
	  ".*\.css$"
	  ;; doesn't play well with prettier
	  ;; ".*\.js$"
	  ".*\.styl?$"
	  ".*\.org?$"
	  ".*\.md$"
	  ".*\.html?$"
	  ))
  (setq auto-save-buffers-enhanced-interval 1.5)
  (setq auto-save-buffers-enhanced-quiet-save-p t)
  (auto-save-buffers-enhanced t))

(define-derived-mode
  xsl-mode nxml-mode "XSL"
  "Nominal XSL mode for org-babel support.")

(require 'prolog)

(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.prg$" . xbase-mode)) ; FoxPro
(add-to-list 'auto-mode-alist '("\\.spr$" . xbase-mode))
(add-to-list 'auto-mode-alist '("\\.h$" . xbase-mode)) ; for now
(add-to-list 'auto-mode-alist '("\\.cs$" . csharp-mode))
(add-to-list 'auto-mode-alist '("\\.bat$" . dos-mode))
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(add-to-list 'auto-mode-alist '("\\.wxs$" . nxml-mode)) ; WiX
(add-to-list 'auto-mode-alist '("\\.xsd$" . nxml-mode))
(add-to-list 'auto-mode-alist '("\\.flow$" . nxml-mode))
(add-to-list 'auto-mode-alist '("\\.aspx$" . nxml-mode)) ; ASP.NET stuff
(add-to-list 'auto-mode-alist '("\\.ascx$" . nxml-mode))
(add-to-list 'auto-mode-alist '("\\.master$" . nxml-mode))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.pl\\'" . prolog-mode)) ;but this is usually perl
(add-to-list 'auto-mode-alist '("\\.prol\\'" . prolog-mode))
;; No, instead write the mode directive in the script
(add-to-list 'auto-mode-alist '("\\.compile\.log$" . compilation-mode)) ; for ws
;; the fset below is still not working
(add-to-list 'auto-mode-alist '("\\.html?$" . nxml-mode))
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.mjs$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.jsx$" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.es6$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.ttl$" . ttl-mode))
(add-to-list 'auto-mode-alist '("\\.sparql$" . sparql-mode))
(add-to-list 'auto-mode-alist '("\\.rnc\\'" . rnc-mode))
(add-to-list 'auto-mode-alist '("\\.ps1\\'" . powershell-mode))
(add-to-list 'auto-mode-alist '("\\.wiki\\'" . creole-mode))
(add-to-list 'auto-mode-alist '("\\.hgignore$" . conf-mode))

(autoload 'rnc-mode "rnc-mode")
(autoload 'moz-minor-mode "moz" "Mozilla Minor and Inferior Mozilla Modes" t)

;; c/o http://stackoverflow.com/a/4988348/4525

;; The important one (AFAICT) is `-f', which means `--force', i.e. don't quit
;; after an error.
(setq sql-mysql-options '("-C" "-t" "-f" "-n"))

(setq-default c-default-style "bsd"
			  c-basic-offset 4
			  tab-width 4
			  fill-column 80)

(setq ring-bell-function 'ignore)
(setq scroll-conservatively 10)
(setq scroll-margin 7)
(setq auto-window-vscroll nil)
(setq redisplay-dont-pause t)			; "obsolete since 24.5"
(setq comint-process-echoes t)        ; don't echo commands in shell
(setq read-buffer-completion-ignore-case t)
(setq compilation-auto-jump-to-first-error t)
 ;;compilation-auto-jump-to-next nil
(setq dired-recursive-deletes 'always)
(setq dired-keep-marker-copy nil)
(setq dired-keep-marker-rename nil)
(setq dired-isearch-filenames t)
										; see doc: "temporarily" turns off case-ignoring if "user input" contains
										; uppercase letters; however, this includes auto-completed input.
(setq iswitchb-case t)
(setq iswitchb-prompt-newbuffer nil)
(setq nxml-sexp-element-flag t)                               ; browse by element

;; narrower dired
(require 'ls-lisp)
;; IS THIS setting only for Windows?
(setq ls-lisp-use-insert-directory-program nil)
(setq ls-lisp-verbosity nil)

;; At some point the fset stopped working, the require fixes it
(require 'nxml-mode)
(fset 'html-mode 'nxml-mode)

(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'cc 'comment-region)
(defalias 'arm 'auto-revert-mode)
(defalias 'rb 'revert-buffer)

(autoload 'kill-ring-search "kill-ring-search"
  "Search the kill ring in the minibuffer."
  (interactive))

(setq display-time-day-and-date t)
(display-time-mode)
(column-number-mode t)
(tooltip-mode nil)
;;(ido-mode t)
(icomplete-mode)
(setq icomplete-prospects-height 3)
(show-paren-mode 1)
(savehist-mode t)
(winner-mode t)
(delete-selection-mode 1)
;;(toggle-uniquify-buffer-names)                        ; it's off by default (or not?)

;; WHY doesn't this happen by default?
(setq custom-file (dot-emacs "custom.el"))
(and (file-exists-p custom-file) (load custom-file))

(global-diff-hl-mode t)

(cd "~")
(shell)

(org-resolve-clocks t)

;; TEMP: dealing with subproject issue in tide mode locating tsserver
(defun gpc/tide-locate-tsserver ()
  "Hack because reasons.
Okay the reason is that `add-node-modules-path' stops looking
after encountering first node project, but typescript itself can
come from a containing project."
  (or
   ;; I've tried a bunch of things, and this (while not perfect) promises to be
   ;; the most reliably useful.  These days I'm living in monorepos where TS is
   ;; installed at the git root but not at the (child) package level.  This
   ;; would be wrong only if individual packages installed their own TS version,
   ;; which will be approximately never.
   (tide--locate-tsserver
	(concat
	 (string-trim-right (shell-command-to-string "git rev-parse --show-toplevel"))
	 "/node_modules/typescript/lib/"))
   ; (tide--locate-tsserver "/Users/GCannizzaro/thrive/web/node_modules/typescript/lib/")
   ; (tide--locate-tsserver "/Users/GCannizzaro/morningstar/app/node_modules/typescript/lib/")
   ; (tide--locate-tsserver "/Users/GCannizzaro/adashi/broadcast-web-app/node_modules/typescript/lib/")
   (tide-tsserver-locater-npmlocal-projectile-npmglobal)
   ))


;; evaluate this to 
(setq tide-tsserver-process-environment '("TSS_LOG=-file /Users/GCannizzaro/research/tsslogs/log.txt -logToFile true -level verbose"))
;; disable when not using
(setq tide-tsserver-process-environment nil)

; probably better as a customization
(setq tide-hl-identifier-idle-time 0.1)

(setq tide-always-show-documentation t)

(defun gpc/tide-reload ()
  "To deal with the ‘selecting deleted buffer’ error that I keep getting."
  (interactive)
  ;; close all buffers that use tide
  (let ((modes-to-close (list 'tide-mode 'web-mode 'js2-mode)))
	;; close all buffers that use one of the above modes
	;; I mean technically you don't have to do this... you can just restart the mode when you get back right?
	;; or reopen the buffer
	;; or, after doing the below you could restart the mode in all of these buffers
	)
  ;; kill all tide servers
  (maphash '(lambda (x y) (progn (message "Killing tide server %s = %s" x y) (delete-process y))) tide-servers)
  (unload-feature 'tide)
  (load "tide")
  ;; maybe restore the killed buffers
  )

;; https://www.emacswiki.org/emacs/KillingBuffers
;; https://emacs.stackexchange.com/questions/16874/list-all-buffers-with-specific-mode

;; This should happen whenever you use agenda multi-occur, but it ain't
;; (dolist (file org-agenda-files) (find-file file))
;; ("~/morningstar/notes/morningstar.org" "~/thrive/notes/thrive.org" "~/adashi/notes/adashi.org" "~/mindgrub/mindgrub-notes/mindgrub.org" "~/nmsf/notes/nmsf.org" "~/viaplace/notes/viaplace.org" "~/noaa/notes/noaa.org" "~/tenjin/notes/tenjin.org" "~/alterwood/notes/alterwood.org")

;;------ written by emacs for "confusing" options
(put 'dired-find-alternate-file 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'upcase-region 'disabled nil)

(put 'downcase-region 'disabled nil)
