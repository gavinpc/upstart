The keyboard repeat rate offered by Windows GUI is not fast enough for me.  I
found this thing online a while ago for setting a faster repeat rate (and
shorter start delay).  I've included the source and also the exe because other
tools would be required to build it.  You could probably build it using only
packages from MSYS2, but I can't say that doing so would make the world a better
place.
