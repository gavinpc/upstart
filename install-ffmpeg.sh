#!/bin/sh

# This won't work if you need lib_aac
# sudo apt-get install -y ffmpeg

# Actually, I'm not sure about that.  I did the following and I still get
# libfdk_aac is not found.  I can use `aac`, but maybe could have done that with
# the above.  This was not the case on the earlier system (trusty), so this is
# still unresolved.

# c/o https://launchpad.net/~mc3man/+archive/ubuntu/xerus-media
#sudo add-apt-repository ppa:mc3man/trusty-media
sudo add-apt-repository ppa:mc3man/xerus-media
sudo apt-get update

# Why was this here?  It probably should be done immediately after install, and
# is probably not directly related to this.
sudo apt-get dist-upgrade

sudo apt-get install -y ffmpeg
